package com.shepherd.zhaw.ui;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.shepherd.zhaw.Shepherd;
import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.ui.helper.ShepherdScreen;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import static java.nio.file.Files.createDirectory;

/**
 * Controller for StartScreen.fxml
 */
public class StartScreenController {

	private static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private FileChooser fileChooser;

	@FXML
	private AnchorPane anchor;

	@FXML
	private void createSchedule() {
		try {
			Stage stage = new ShepherdStage(anchor.getScene());
			FXMLLoader loader = ShepherdScreen.getLoader(ShepherdScreen.CREATE_NEW_SCHEDULE_SCREEN);
			stage.setScene(new Scene(loader.load()));
			stage.setResizable(false);
			stage.show();
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, "Could not open CreateNewScheduleScreen", ex);
		}
	}

	@FXML
	private void openSchedule() {
		Stage stage = (Stage) anchor.getScene().getWindow();
		fileChooser = new FileChooser();

		String currentlocation = Paths.get(".").toAbsolutePath().normalize().toString();
		String plannedMonthsDirectory = currentlocation + File.separator + Config.getData().getPlannedMonthsFolder();

		// create result-folder, if not exists
		File file = new File(plannedMonthsDirectory);
		if (!file.exists()) {
            try {
                createDirectory(file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

		fileChooser.setInitialDirectory(new File(plannedMonthsDirectory));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Excel", Shepherd.ALLOWED_EXTENSION));
		File selectedFile = fileChooser.showOpenDialog(stage);
		try {
			if (selectedFile != null) {
				Desktop.getDesktop().open(selectedFile);
			}
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, "Could not open Schedule", ex);
		}
	}
}