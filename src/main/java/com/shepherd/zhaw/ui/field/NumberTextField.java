package com.shepherd.zhaw.ui.field;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

import java.util.regex.Pattern;

/**
 * NumberTextField. Only numbers allowed in text field. Parameters: min, max
 * value
 */
public class NumberTextField extends TextField implements ChangeListener<String> {

	private static final String STRING_PATTERN = "[0-9]*";
	private static final Pattern PATTERN = Pattern.compile(STRING_PATTERN);
	private static final NumberStringConverter NS_CONVERTER = new NumberStringConverter();

	private boolean hasBeenInitalized = false;
	private int min;
	private int max;

	@Override
	public void replaceText(int start, int end, String text) {
		if (validate(text)) {
			super.replaceText(start, end, text);
		}
	}

	@Override
	public void replaceSelection(String text) {
		if (validate(text)) {
			super.replaceSelection(text);
		}
	}

	private boolean validate(String text) {
		return text.matches(STRING_PATTERN);
	}

	public NumberTextField setMin(int min) {
		this.min = min;
		return this;
	}

	public NumberTextField setMax(int max) {
		this.max = max;
		return this;
	}

	private void init() {
		if (hasBeenInitalized) {
			return;
		}
		textProperty().addListener(this);
	}

	@Override
	protected void layoutChildren() {
		if (!hasBeenInitalized) {
			init();
		}
		super.layoutChildren();
	}

	@Override
	public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		Number value;
		if (PATTERN.matcher(newValue).matches()) {
			value = NS_CONVERTER.fromString(newValue);
			if (value != null) {
				if (value.longValue() > max) {
					setText(String.valueOf(max));
				} else if (value.longValue() < min) {
					setText(String.valueOf(min));
				}
			}
		} else if (!newValue.isEmpty()) {
			setText(oldValue);
		}
	}
}
