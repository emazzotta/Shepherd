package com.shepherd.zhaw.ui;

import com.shepherd.zhaw.ui.dialog.WarningDialog;
import com.shepherd.zhaw.ui.helper.ShepherdScreen;
import com.shepherd.zhaw.ui.model.ScheduleData;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;

/**
 * Controller for CreateNewScheduleScreen.fxml
 */
public class CreateScheduleController implements Initializable {

	private ResourceBundle resources;

	@FXML
	private AnchorPane anchorPane;

	@FXML
	private TextField scheduleName;

	@FXML
	private ComboBox<String> monthList;

	@FXML
	private ComboBox<Integer> yearList;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.resources = resources;
		for (int i = 0; i < 12; i++) {
			monthList.getItems().add(i, resources.getString("label.month." + String.valueOf(i + 1)));
		}
		monthList.getSelectionModel().select(Calendar.getInstance().get(Calendar.MONTH) + 1);

		yearList.getItems().add(Calendar.getInstance().get(Calendar.YEAR));
		yearList.getSelectionModel().select(0);
	}

	@FXML
	private void editStaff() {
		ShepherdScreen.openEditStaffScreen(anchorPane.getScene());
	}

	@FXML
	private void defineRules() {
		ShepherdScreen.openRulesScreen(anchorPane.getScene());
	}

	@FXML
	private void generateSchedule() {
		// check fields
		if (scheduleName.getText().isEmpty() || monthList.getSelectionModel().isEmpty()
				|| yearList.getSelectionModel().isEmpty()) {
			WarningDialog dialog = new WarningDialog(resources.getString("label.warning"),
					resources.getString("warning.fillOutAllFields"));
			dialog.show();
		} else {
			ScheduleData scheduleData = new ScheduleData(scheduleName.getText(),
					monthList.getSelectionModel().getSelectedIndex(), yearList.getSelectionModel().getSelectedItem());
			Stage stage = (Stage) anchorPane.getScene().getWindow();
			stage.close();
			ShepherdScreen.openProgressScreen(stage.getOwner().getScene(), scheduleData);
		}
	}
}