package com.shepherd.zhaw.ui.model;

/**
 * Schedule data to pass from UI to Scheduler
 */
public class ScheduleData {

	private String scheduleName;
	private int month;
	private int year;

	public ScheduleData() {

	}

	public ScheduleData(String scheduleName, int month, int year) {
		this.scheduleName = scheduleName;
		this.month = month;
		this.year = year;
	}

	//
	public boolean isAllFieldsSet() {
		return !scheduleName.isEmpty() && month != 0 && year != 0;
	}

	//
	public String getScheduleName() {
		return scheduleName;
	}

	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ScheduleData [scheduleName=");
		builder.append(scheduleName);
		builder.append(", month=");
		builder.append(month);
		builder.append(", year=");
		builder.append(year);
		builder.append("]");
		return builder.toString();
	}
}
