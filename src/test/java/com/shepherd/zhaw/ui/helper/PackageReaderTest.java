package com.shepherd.zhaw.ui.helper;

import org.junit.Assert;
import org.junit.Test;

public class PackageReaderTest {

	@Test
	public void shouldReadOnClass() throws Exception {
		Class<?>[] clazzes = PackageReader.getClasses("com.shepherd.zhaw.config");
		Assert.assertEquals(6, clazzes.length);
	}

}
