package com.shepherd.zhaw.scheduling;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.export.PlannedMonthExport;
import com.shepherd.zhaw.scheduling.exceptions.SchedulingCouldNotBeFinishedException;
import com.shepherd.zhaw.scheduling.strategies.EvaluationStrategy;

import java.util.Calendar;
import java.util.Date;

/**
 * Combines the calculated planned days to a planned month
 */
public class SchedulerMonth {
	private Date startDate, endDate;
	private String plannedMonthName;
	
	public SchedulerMonth(Date startDate, Date endDate, String plannedMonthName) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.plannedMonthName = plannedMonthName;
	} 
	
	/**
	 * Loops through all days on the month to plan and combines the calculated planned days to a planned month
	 * @param the strategy to be used to calculate a planned day
	 * @return the calculated planned month
	 * @throws if a month can't be planned a SchedulingCouldNotBeFinishedException is thrown
	 */
	public PlannedMonth schedule(EvaluationStrategy evaluationStrategy) throws SchedulingCouldNotBeFinishedException {
		PlannedMonth plannedMonth = new PlannedMonth(plannedMonthName);
		evaluationStrategy.setPlannedMonth(plannedMonth);
		
		Date dateToPlan = (Date)startDate.clone();
		while (dateToPlan.getTime()  <= endDate.getTime()) {
			try {
				plannedMonth.addPlannedDay(dateToPlan, SchedulerDay.schedule(dateToPlan, plannedMonth, evaluationStrategy));
			} catch (SchedulingCouldNotBeFinishedException ex) {
				if (Config.getData().isDebugMode()) {
					PlannedMonthExport.export(plannedMonth, "debug");
				}
				throw ex;
			}
			dateToPlan = nextDay(dateToPlan);
		}
		return plannedMonth;
	}
	
	/**
	 * Returns the next day 
	 * @param the current date
	 * @return the next day
	 */
	private Date nextDay(Date date) {
		Calendar calendar = Calendar.getInstance(); 
		calendar.setTime(date); 
		calendar.add(Calendar.DATE, 1);
		return calendar.getTime();
	}
}
