package com.shepherd.zhaw.constraints.weighted;

import com.shepherd.zhaw.constraints.ConstraintInput;
import com.shepherd.zhaw.scheduling.PlannedDay;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * Tries to distribute shifts evenly between employees.
 */
public class PreventSameShift extends WeightedConstraint {

	/**
	 * Returns an integer value that rates the shift to be planned
	 * @param input contains information about all shifts planned so far and the current shift to be planned
	 * @return number defining how good the plan would be with the shift to be planned
	 */
	@Override
	public int rate(ConstraintInput input) {

		boolean employeeOnShiftStreak = false;
		Date previousDay = new Date();
        previousDay.setTime(input.getDate().getTime() - (long)1*1000*60*60*24 );
        HashMap<Date, PlannedDay> days = input.getPlannedMonth().getPlannedDays();
        if(days.containsKey(previousDay)) {
            PlannedDay day = days.get(previousDay);
            if(day.getAllPlannedEmployees().contains(input.getEmployee())) {
            	employeeOnShiftStreak = true; // a shift streak shoundn't be aborted 
            }
        }

		ArrayList<PlannedDay> plannedDaysByEmployee = input.getPlannedMonth().getPlannedDaysByEmployee(input.getEmployee());
		int plannedDays = plannedDaysByEmployee.size();
        
        int rating = 0;
        if (!employeeOnShiftStreak && plannedDays > 0) {
			int countShiftsByEmployee = 0; 
			for (PlannedDay plannedDay : plannedDaysByEmployee) {
				if (plannedDay.getPlannedEmployeesByShiftType(input.getShiftType()).contains(input.getEmployee())) {
					countShiftsByEmployee++;
				}
			}
	
			double shiftRatio = (double)countShiftsByEmployee/(double)plannedDays;
			rating = (int)Math.round((shiftRatio - 0.333) * 10); // one third is the optimum and should get therefore a zero rating 
        }
		return rating;
	}
}
