package com.shepherd.zhaw.ui;

import com.shepherd.zhaw.constraints.ConstraintFactory;
import com.shepherd.zhaw.ui.comparator.ConstraintTypeModelComparator;
import com.shepherd.zhaw.ui.dialog.WarningDialog;
import com.shepherd.zhaw.ui.model.ConstraintTypeModel;
import com.shepherd.zhaw.ui.model.RulesModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for EditRuleScreen.fxml
 */
public class EditRuleController implements Initializable {

	private RulesModel rulesModel;
	private ResourceBundle resources;

	@FXML
	private AnchorPane anchorPane;

	@FXML
	private TextField name;

	@FXML
	private ComboBox<ConstraintTypeModel> type;

	@FXML
	private TextArea settings;

	public EditRuleController(RulesModel rulesModel) {
		this.rulesModel = rulesModel;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.resources = resources;

		if (!rulesModel.isEmpty()) {
			name.setText(rulesModel.getName());
			SingleSelectionModel<ConstraintTypeModel> typeModel = type.getSelectionModel();
			typeModel.select(rulesModel.getConstraintType());
			type.setSelectionModel(typeModel);
			settings.setText(rulesModel.getSettings());
		}

		ObservableList<ConstraintTypeModel> constraintTypes = FXCollections.observableArrayList();
		constraintTypes.addAll(ConstraintFactory.getConstraintTypes());
		constraintTypes.sort(new ConstraintTypeModelComparator());
		type.setItems(constraintTypes);

	}

	@FXML
	public void saveAndClose() {
		// check inputs
		if (name.getText().isEmpty() || type.getSelectionModel().getSelectedItem() == null
				|| settings.getText().isEmpty()) {
			WarningDialog dialog = new WarningDialog(resources.getString("label.warning"),
					resources.getString("warning.fillOutAllFields"));
			dialog.show();
		}

		else {
			rulesModel.setName(name.getText());
			rulesModel.setConstraintType(type.getSelectionModel().getSelectedItem());
			rulesModel.setSettings(settings.getText());

			// close window
			Stage stage = (Stage) anchorPane.getScene().getWindow();
			stage.close();
		}
	}
}