package com.shepherd.zhaw.ui.comparator;

import java.io.Serializable;
import java.util.Comparator;

import com.shepherd.zhaw.ui.model.RulesModel;

public class RulesModelComparator implements Comparator<RulesModel>, Serializable {

    private static final long serialVersionUID = 1;

	@Override
	public int compare(RulesModel o1, RulesModel o2) {
		return o1.getName().compareTo(o2.getName());
	}

}
