package com.shepherd.zhaw.ui.model;

import com.shepherd.zhaw.config.Employee;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Model for Employee-Screen
 */
public class EmployeeModel {

	private final SimpleStringProperty firstname;
	private final SimpleStringProperty lastname;
	private final SimpleIntegerProperty worktimePercent;

	public EmployeeModel() {
		this.firstname = new SimpleStringProperty();
		this.lastname = new SimpleStringProperty();
		this.worktimePercent = new SimpleIntegerProperty();
	}

	public EmployeeModel(Employee employee) {
		this.firstname = new SimpleStringProperty(employee.getFirstname());
		this.lastname = new SimpleStringProperty(employee.getLastname());
		this.worktimePercent = new SimpleIntegerProperty(employee.getWorktimePercent());
	}

	// methods
	public boolean allFieldsSet() {
		return !firstname.isEmpty().get() && !lastname.isEmpty().get();
	}

	// getters and setters
	public void setFirstname(String fname) {
		firstname.set(fname);
	}

	public String getFirstname() {
		return firstname.get();
	}

	public SimpleStringProperty getFirstnameProperty() {
		return firstname;
	}

	public void setLastname(String lname) {
		lastname.set(lname);
	}

	public String getLastname() {
		return lastname.get();
	}

	public SimpleStringProperty getLastnameProperty() {
		return lastname;
	}

	public void setWorktimePercent(String percent) {
		if (percent != null && !percent.isEmpty()) {
			worktimePercent.set(Integer.valueOf(percent));
		}
	}

	public int getWorktimePercent() {
		return worktimePercent.get();
	}

	public SimpleIntegerProperty getWorktimePercentProperty() {
		return worktimePercent;
	}

	public Employee getEmployee() {
		return new Employee(firstname.get(), lastname.get(), worktimePercent.get());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeModel [firstname=");
		builder.append(firstname);
		builder.append(", lastname=");
		builder.append(lastname);
		builder.append(", worktimePercent=");
		builder.append(worktimePercent);
		builder.append("]");
		return builder.toString();
	}
}
