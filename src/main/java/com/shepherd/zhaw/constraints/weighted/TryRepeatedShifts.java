package com.shepherd.zhaw.constraints.weighted;

import com.shepherd.zhaw.constraints.ConstraintInput;
import com.shepherd.zhaw.scheduling.PlannedDay;

import java.util.Date;
import java.util.HashMap;

/**
 * Returns better rating values if the same shift for the same employee has been scheduled
 * the last day.
 */
public class TryRepeatedShifts extends WeightedConstraint {

    /**
     * Returns an integer value that rates the shift to be planned
     * @param input contains information about all shifts planned so far and the current shift to be planned
     * @return number defining how good the plan would be with the shift to be planned
     */
    public int rate(ConstraintInput input) {
        Date previousDay = new Date();
        previousDay.setTime(input.getDate().getTime() - (long)1*1000*60*60*24 );
        // Find previous day and check if shift was the same for current employee

        HashMap<Date, PlannedDay> days = input.getPlannedMonth().getPlannedDays();
        if(days.containsKey(previousDay)) {
            PlannedDay day = days.get(previousDay);
            if(day.getPlannedEmployeesByShiftType(input.getShiftType()).contains(input.getEmployee())) {
            	
            	// Employee worked in the same shift yesterday - check if this has been the case for the last 4 days
            	for(int i = 1; i < 3; i++) {
            		Date date = new Date();
            		date.setTime(input.getDate().getTime() - (long)i*1000*60*60*24);
                    
            		if(!days.containsKey(date))
            			return 0;
            		
            		day = days.get(date);
            		
                    if(!day.getPlannedEmployeesByShiftType(input.getShiftType()).contains(input.getEmployee()))
                    		return 0;
            	}
            
                return 25;
            }
        }
        
        return 25;
    }

}
