package com.shepherd.zhaw.constraints.mandatory;

import com.shepherd.zhaw.constraints.Constraint;
import com.shepherd.zhaw.constraints.ConstraintInput;

/**
 * All constraints which are required (for example, by law or organizational) inherit from MandatoryContstraint.
 */
public abstract class MandatoryConstraint extends Constraint {

    /**
     * Returns true if the shift to be planned conforms with this constraint
     * @param input contains information about all shifts planned so far and the current shift to be planned
     * @return if shift to be planned is conform with this constraint
     */
    public abstract boolean isValid(ConstraintInput input);

}
