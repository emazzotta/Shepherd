package com.shepherd.zhaw.scheduling;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.config.ShiftType;
import com.shepherd.zhaw.scheduling.exceptions.SchedulingCouldNotBeFinishedException;
import com.shepherd.zhaw.scheduling.strategies.EvaluationStrategy;

import java.util.*;

/**
 * Calculates a plan for a single day
 */
public class SchedulerDay {

	/**
	 * Calculates a plan for a single day
	 * @param date of the day to be planned
	 * @param the planned month in which the day should be planned
	 * @param the strategy which should be used to calculate the plan
	 * @return the calculated PlannedDay
	 * @throws if no plan can be calculated a SchedulingCouldNotBeFinishedException is thrown
	 */
	public static PlannedDay schedule(Date date, PlannedMonth plannedMonth, EvaluationStrategy evaluationStrategy)
			throws SchedulingCouldNotBeFinishedException {
		PlannedDay plannedDay = new PlannedDay(date);

		TreeSet<TemporaryAssignment> weightedAssignments = getWeightedAssignments(date, plannedMonth, evaluationStrategy);
		Iterator<TemporaryAssignment> assignmentIterator = weightedAssignments.iterator();
		HashSet<Employee> plannedEmployees = new HashSet<Employee>();
		int shiftSlotsToPlan = getShiftSlotsToPlan();
		while (assignmentIterator.hasNext() && shiftSlotsToPlan > 0) {
			TemporaryAssignment assignment = assignmentIterator.next();

			if (!plannedEmployees.contains(assignment.getEmployee())) {
				if (plannedDay.getPlannedEmployeesByShiftType(assignment.getShiftType()).size() * 100 < assignment
						.getShiftType().getNeededWorkforcePercent()) {
					plannedDay.addPlannedEmployee(assignment.getShiftType(), assignment.getEmployee());
					int weight = assignment.getAssignementWeight();
					plannedDay.addWeight(weight);
					plannedEmployees.add(assignment.getEmployee());
					shiftSlotsToPlan--;
				}
			}
		}

		if (shiftSlotsToPlan > 0) {
			throw new SchedulingCouldNotBeFinishedException();
		}

		return plannedDay;
	}

	/**
	 * How many shift slots have to be planned
	 * @return the amount of shift slots to be planned
	 */
	private static int getShiftSlotsToPlan() {
		int shiftsWorkforcePercent = 0;
		for (ShiftType shiftType : Config.getData().getShiftTypes()) {
			shiftsWorkforcePercent += shiftType.getNeededWorkforcePercent();
		}
		return shiftsWorkforcePercent / 100;
	}

	/**
	 * Calcuates for all employees and shifts ratings and returns a TreeMap sorted by the given strategy
	 * @param the date for which the ratings should be calculated
	 * @param the planned month to consider
	 * @param the strategy to sort the Set
	 * @return a TreeMap sorted by the given strategy
	 */
	private static TreeSet<TemporaryAssignment> getWeightedAssignments(Date date, PlannedMonth plannedMonth,
		EvaluationStrategy evaluationStrategy) {
		TreeSet<TemporaryAssignment> weightedAssignments = new TreeSet<TemporaryAssignment>(evaluationStrategy);

		List<Employee> employees = Config.getData().getEmployees();
		ShiftType[] shiftTypes = Config.getData().getShiftTypes();

		for (Employee employee : employees) {
			for (ShiftType shiftType : shiftTypes) {
				if (employee.isValid(plannedMonth, date, shiftType)) {
					weightedAssignments.add(new TemporaryAssignment(employee, shiftType, employee.getPlanWeight(plannedMonth, date, shiftType)));
				}
			}
		}
		return weightedAssignments;
	}
}
