package com.shepherd.zhaw.constraints.weighted;

import com.shepherd.zhaw.constraints.Constraint;
import com.shepherd.zhaw.constraints.ConstraintInput;

/**
 * All constraints that are weighted (not mandatory) inherit from WeightedConstraints.
 */
public abstract class WeightedConstraint extends Constraint {

    /**
     * Returns an integer value that rates the shift to be planned
     * @param input contains information about all shifts planned so far and the current shift to be planned
     * @return number defining how good the plan would be with the shift to be planned
     */
    public abstract int rate(ConstraintInput input);

}
