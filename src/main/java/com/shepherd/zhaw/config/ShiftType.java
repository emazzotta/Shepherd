package com.shepherd.zhaw.config;

public class ShiftType {
	private String name;
	private int neededWorkforcePercent;
	
	public ShiftType(String name, int neededWorkforcePercent) {
		this.name = name;
		this.neededWorkforcePercent = neededWorkforcePercent;
	}
	
	public char getShortName() {
	    return name.charAt(0);
    }
	
	public int getNeededWorkforcePercent() {
		return neededWorkforcePercent;
	}
}
