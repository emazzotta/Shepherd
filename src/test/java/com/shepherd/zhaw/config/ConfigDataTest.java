package com.shepherd.zhaw.config;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConfigDataTest {

	@Test
	public void shouldInitWithDefaultValues() {
		ConfigData data = new ConfigData();
		data.initDefaultValues();
		assertEquals(data.getShiftTypes().length, 3);
	}
}
