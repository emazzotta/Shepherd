package com.shepherd.zhaw.ui.comparator;

import java.io.Serializable;
import java.util.Comparator;

import com.shepherd.zhaw.ui.model.ConstraintTypeModel;

public class ConstraintTypeModelComparator implements Comparator<ConstraintTypeModel>, Serializable {

    private static final long serialVersionUID = 1;

	@Override
	public int compare(ConstraintTypeModel c1, ConstraintTypeModel c2) {
		return c1.getClazz().getSimpleName().compareTo(c2.getClazz().getSimpleName());
	}

}
