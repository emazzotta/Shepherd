package com.shepherd.zhaw.ui;

import com.shepherd.zhaw.export.PlannedMonthExport;
import com.shepherd.zhaw.scheduling.PlannedMonth;
import com.shepherd.zhaw.scheduling.Scheduler;
import com.shepherd.zhaw.ui.model.ScheduleData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller for ShowProgressScreen.fxml
 */
public class ProgressScreenController implements Initializable {

	private static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private OpenExcelTask openExcelTask;
	private Scheduler schedulingTask;
	private ScheduleData scheduleData;

	@FXML
	private ImageView progressLogo;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private Label progressInfo;

	@FXML
	private Label progressLabel;

	@FXML
	private Button cancelButton;

	public ProgressScreenController(ScheduleData scheduleData) {
		this.scheduleData = scheduleData;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		schedulingTask = new Scheduler(scheduleData.getMonth(), scheduleData.getYear(), scheduleData.getScheduleName(),
				resources);
		new Thread(schedulingTask).start();

		progressBar.progressProperty().unbind();
		progressBar.progressProperty().bind(schedulingTask.progressProperty());

		schedulingTask.messageProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				progressLabel.setText(newValue);
			}
		});

		schedulingTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent t) {
				PlannedMonth plannedMonth = schedulingTask.getValue();
				if (plannedMonth != null) {
					String filePath = PlannedMonthExport.export(plannedMonth);
					openExcelTask = new OpenExcelTask(filePath);
					new Thread(openExcelTask).start();

					progressLabel
							.setText(MessageFormat.format(resources.getString("label.progress.excelOpened"), filePath));

				} else {
					progressLabel.setText(resources.getString("label.progress.unsuccessful"));

					progressBar.progressProperty().unbind();
					progressBar.setProgress(1.0);

					progressLogo.setImage(new Image("/logo/shepherd_logo_200-200.png"));
					progressLogo.setFitWidth(150);
					progressLogo.setFitHeight(150);
				}

				progressInfo.setVisible(false);
				cancelButton.setText(resources.getString("action.close"));
			}
		});
	}

	@FXML
	private void cancelScheduling() {
		schedulingTask.cancel(true);
		progressBar.progressProperty().unbind();
		progressBar.setProgress(0);
		Stage stage = (Stage) cancelButton.getScene().getWindow();
		stage.close();
	}

	/**
	 * Opens the generated schedule as xlsx
	 */
	private class OpenExcelTask extends Task<Void> {

		private String filePath;

		public OpenExcelTask(String filePath) {
			this.filePath = filePath;
		}

		@Override
		protected Void call() throws Exception {
			try {
				Desktop.getDesktop().open(new File(filePath));
			} catch (IOException ex) {
				LOGGER.log(Level.SEVERE, "Excel-Schedule not found!", ex);
			}
			return null;
		}

		@Override
		protected void succeeded() {

			progressBar.progressProperty().unbind();
			progressBar.setProgress(1.0);

			progressLogo.setImage(new Image("/logo/shepherd_logo_200-200.png"));
			progressLogo.setFitWidth(150);
			progressLogo.setFitHeight(150);

			super.succeeded();
		}
	}
}