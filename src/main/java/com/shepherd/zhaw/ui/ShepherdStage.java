package com.shepherd.zhaw.ui;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Shepherd Stage with specific title and icon
 */
public class ShepherdStage extends Stage {

	public ShepherdStage() {
		setTitle("Shepherd");
		getIcons().add(new Image("/logo/shepherd_logo_200-200.png"));
	}

	public ShepherdStage(Scene owner) {
		this();
		if (owner != null) {
			initModality(Modality.WINDOW_MODAL);
			initOwner(owner.getWindow());
		}
	}

}
