package com.shepherd.zhaw.ui;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.ui.comparator.EmployeeModelComparator;
import com.shepherd.zhaw.ui.dialog.WarningDialog;
import com.shepherd.zhaw.ui.field.NumberTextField;
import com.shepherd.zhaw.ui.model.EmployeeModel;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for ManageStaffScreen.fxml
 */
public class ManageStaffScreenController implements Initializable {

	private ObservableList<EmployeeModel> dataList;
	private EmployeeModel employeeToEdit;
	private ResourceBundle resources;

	@FXML
	private TableView<EmployeeModel> employeeTable;

	@FXML
	private TableColumn<EmployeeModel, String> firstnameColumn;

	@FXML
	private TableColumn<EmployeeModel, String> lastnameColumn;

	@FXML
	private TableColumn<EmployeeModel, Integer> workpercentColumn;

	@FXML
	private TableColumn<EmployeeModel, EmployeeModel> editColumn;

	@FXML
	private TableColumn<EmployeeModel, EmployeeModel> deleteColumn;

	@FXML
	private GridPane editPane;

	@FXML
	private TextField firstnameField;

	@FXML
	private TextField lastnameField;

	@FXML
	private Button cancelButton;

	@FXML
	private NumberTextField workpercentField;

	public ManageStaffScreenController() {
		this.dataList = FXCollections.observableArrayList();
		for (Employee employee : Config.getData().getEmployees()) {
			dataList.add(new EmployeeModel(employee));
		}
		dataList.sort(new EmployeeModelComparator());
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.resources = resources;

		editPane.setVisible(false);

		cancelButton.setVisible(false);
		employeeTable.setItems(dataList);
		workpercentField.setMin(0).setMax(100);

		// init columns
		lastnameColumn.setCellValueFactory(cellData -> cellData.getValue().getLastnameProperty());
		lastnameColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<EmployeeModel, String>>() {

			@Override
			public void handle(CellEditEvent<EmployeeModel, String> event) {
				event.getTableView().getItems().get(event.getTablePosition().getRow()).setLastname(event.getNewValue());
			}
		});

		firstnameColumn.setCellValueFactory(cellData -> cellData.getValue().getFirstnameProperty());
		firstnameColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<EmployeeModel, String>>() {

			@Override
			public void handle(CellEditEvent<EmployeeModel, String> event) {
				event.getTableView().getItems().get(event.getTablePosition().getRow())
						.setFirstname(event.getNewValue());
			}
		});

		workpercentColumn.setCellValueFactory(cellData -> cellData.getValue().getWorktimePercentProperty().asObject());

		// edit
		editColumn.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		editColumn.setCellFactory(param -> new TableCell<EmployeeModel, EmployeeModel>() {

			private final Button editButton = new Button(resources.getString("action.edit"));

			@Override
			protected void updateItem(EmployeeModel employee, boolean empty) {
				super.updateItem(employee, empty);
				if (employee == null) {
					setGraphic(null);
				} else {
					setGraphic(editButton);
					editButton.setOnAction(event -> resetEditFields(employee));
				}
			}
		});

		// delete
		deleteColumn.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		deleteColumn.setCellFactory(param -> new TableCell<EmployeeModel, EmployeeModel>() {

			private final Button deleteButton = new Button(resources.getString("action.delete"));

			@Override
			protected void updateItem(EmployeeModel employee, boolean empty) {
				super.updateItem(employee, empty);
				if (employee == null) {
					setGraphic(null);
				} else {
					setGraphic(deleteButton);
					deleteButton.setOnAction(event -> getTableView().getItems().remove(employee));
				}
			}
		});
	}

	private void resetEditFields(EmployeeModel employeeModel) {
		this.employeeToEdit = employeeModel;
		if (employeeModel != null) {
			editPane.setVisible(true);
			firstnameField.setText(employeeModel.getFirstname());
			lastnameField.setText(employeeModel.getLastname());
			workpercentField.setText(String.valueOf(employeeModel.getWorktimePercent()));
			cancelButton.setVisible(true);
		} else {
			editPane.setVisible(false);
			firstnameField.setText("");
			lastnameField.setText("");
			workpercentField.setText("");
			cancelButton.setVisible(false);
		}
	}

	@FXML
	public void cancelEditing() {
		resetEditFields(null);
	}

	@FXML
	public void newEmployee() {
		resetEditFields(null);
		editPane.setVisible(true);
	}

	@FXML
	public void saveEmployee() {
		// check inputs
		if (firstnameField.getText().isEmpty() || lastnameField.getText().isEmpty()
				|| workpercentField.getText().isEmpty()) {
			WarningDialog dialog = new WarningDialog(resources.getString("label.warning"),
					resources.getString("warning.fillOutAllFields"));
			dialog.show();
		}

		else {
			boolean newEmployee = false;
			if (employeeToEdit == null) {
				employeeToEdit = new EmployeeModel();
				newEmployee = true;
			}
			employeeToEdit.setFirstname(firstnameField.getText());
			employeeToEdit.setLastname(lastnameField.getText());
			employeeToEdit.setWorktimePercent(workpercentField.getText());
			if (employeeToEdit.allFieldsSet()) {
				if (newEmployee) {
					dataList.add(employeeToEdit);
				}
				resetEditFields(null);
			}
		}
	}

	@FXML
	public void saveEmployees() {
		List<Employee> employees = new ArrayList<>();
		dataList.forEach(c -> employees.add(c.getEmployee()));
		Config.getData().setEmployees(employees);
		Config.storeData();
		// close window
		Stage stage = (Stage) employeeTable.getScene().getWindow();
		stage.close();
	}
}