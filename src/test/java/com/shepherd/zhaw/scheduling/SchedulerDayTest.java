package com.shepherd.zhaw.scheduling;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.ConfigData;
import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.config.ShiftType;
import com.shepherd.zhaw.scheduling.exceptions.SchedulingCouldNotBeFinishedException;
import com.shepherd.zhaw.scheduling.strategies.BestEffortEvaluationStrategy;
import com.shepherd.zhaw.scheduling.strategies.EvaluationStrategy;

public class SchedulerDayTest {

	private PlannedMonth plannedMonth;
	private EvaluationStrategy strategy;
	
	@Before
	public void setUp() throws Exception {
		plannedMonth = new PlannedMonth("Test");
		strategy = new BestEffortEvaluationStrategy();
		strategy.setPlannedMonth(plannedMonth);
	}

	@Test(expected=SchedulingCouldNotBeFinishedException.class)
	public void shouldThrowExceptionIfNotEngoughEmployees() throws SchedulingCouldNotBeFinishedException {
		ConfigData configData = new ConfigData();
		configData.initDefaultValues();
		Config.setDataForTests(configData);
		
		SchedulerDay.schedule(new Date(), plannedMonth, strategy);
	}
	
	@Test(expected=SchedulingCouldNotBeFinishedException.class)
	public void shouldThrowExceptionIfNotEngoughEmployees2() throws SchedulingCouldNotBeFinishedException {
		ConfigData configData = new ConfigData();
		configData.initDefaultValues();
		
		Employee a = new Employee("A", "A", 100);
		Employee b = new Employee("B", "B", 100);
		Employee c = new Employee("C", "C", 100);
		Employee d = new Employee("D", "D", 100);
		Employee e = new Employee("E", "E", 100);
		
		ArrayList<Employee> employees = new ArrayList<>();
		employees.add(a);
		employees.add(b);
		employees.add(c);
		employees.add(d);
		employees.add(e);
		configData.setEmployees(employees);
		
		Config.setDataForTests(configData);
		
		PlannedDay plannedDay = SchedulerDay.schedule(new Date(), plannedMonth, strategy);
		ArrayList<Employee> plannedEmployees = plannedDay.getAllPlannedEmployees();
	}

	@Test
	public void shouldAddAllEmployeesIfJustEnoughEmployees() throws SchedulingCouldNotBeFinishedException {
		ConfigData configData = new ConfigData();
		configData.initDefaultValues();
		
		Employee a = new Employee("A", "A", 100);
		Employee b = new Employee("B", "B", 100);
		Employee c = new Employee("C", "C", 100);
		Employee d = new Employee("D", "D", 100);
		Employee e = new Employee("E", "E", 100);
		Employee f = new Employee("F", "F", 100);
		
		ArrayList<Employee> employees = new ArrayList<>();
		employees.add(a);
		employees.add(b);
		employees.add(c);
		employees.add(d);
		employees.add(e);
		employees.add(f);
		configData.setEmployees(employees);
		
		Config.setDataForTests(configData);
		
		PlannedDay plannedDay = SchedulerDay.schedule(new Date(), plannedMonth, strategy);
		ArrayList<Employee> plannedEmployees = plannedDay.getAllPlannedEmployees();
		assertTrue(plannedEmployees.contains(a));
		assertTrue(plannedEmployees.contains(b));
		assertTrue(plannedEmployees.contains(c));
		assertTrue(plannedEmployees.contains(d));
		assertTrue(plannedEmployees.contains(e));
		assertTrue(plannedEmployees.contains(f));
	}
	
	@Test
	public void shouldFillAllShiftsAccordingly() throws SchedulingCouldNotBeFinishedException {
		ConfigData configData = new ConfigData();
		configData.initDefaultValues();
		
		Employee a = new Employee("A", "A", 100);
		Employee b = new Employee("B", "B", 100);
		Employee c = new Employee("C", "C", 100);
		Employee d = new Employee("D", "D", 100);
		Employee e = new Employee("E", "E", 100);
		Employee f = new Employee("F", "F", 100);
		
		ArrayList<Employee> employees = new ArrayList<>();
		employees.add(a);
		employees.add(b);
		employees.add(c);
		employees.add(d);
		employees.add(e);
		employees.add(f);
		configData.setEmployees(employees);
		
		Config.setDataForTests(configData);
		
		PlannedDay plannedDay = SchedulerDay.schedule(new Date(), plannedMonth, strategy);
		for (ShiftType shiftType : configData.getShiftTypes()) {
			int shiftSlots = shiftType.getNeededWorkforcePercent() / 100;
			assertEquals(shiftSlots, plannedDay.getPlannedEmployeesByShiftType(shiftType).size());
		}
	}
}
