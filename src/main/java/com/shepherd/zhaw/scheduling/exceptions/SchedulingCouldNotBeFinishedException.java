package com.shepherd.zhaw.scheduling.exceptions;

/**
 * Indicates that a day couldn't be planned
 */
public class SchedulingCouldNotBeFinishedException extends Exception {

	private static final long serialVersionUID = 1L;

}
