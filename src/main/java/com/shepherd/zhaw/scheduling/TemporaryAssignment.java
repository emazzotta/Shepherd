package com.shepherd.zhaw.scheduling;

import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.config.ShiftType;

/**
 * Stores the data of a temporary assignment 
 */
public class TemporaryAssignment {
	private Employee employee;
	private ShiftType shiftType;
	private int assignementWeight;
	
	public TemporaryAssignment(Employee employee, ShiftType shiftType, int assignementWeight) {
		this.employee = employee;
		this.shiftType = shiftType;
		this.assignementWeight = assignementWeight;
	}
	
	/**
	 * Returns the employee of the temporary assignment 
	 * @return employee of the temporary assignment 
	 */
	public Employee getEmployee() {
		return employee;
	}
	
	/**
	 * Returns the shift type of the temporary assignment 
	 * @return shift type of the temporary assignment 
	 */
	public ShiftType getShiftType() {
		return shiftType;
	}

	/**
	 * Returns the assignment weight of the temporary assignment 
	 * @return assignment weight of the temporary assignment
	 */
	public int getAssignementWeight() {
		return assignementWeight;
	}
}

