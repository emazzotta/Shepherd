# Shepherd build file.
#
# All commands necessary to go from development to release candidate should be here.

CURRENT_DIR = $(shell pwd)
export PATH := $CURRENT_DIR:$(PATH)

.PHONY: all
all: build artifact

.PHONY: build
build:
	@mvn clean compile

.PHONY: test
test:
	@mvn test

.PHONY: artifact
artifact:
	@mvn clean install

.PHONY: zip 
zip:
	@zip -r shepherd_source_code.zip README.md config.json Makefile pom.xml .env.example src

.PHONY: bugreport
bugreport:
	@mvn clean compile site
	@mvn findbugs:gui

.PHONY: run
run:
	@java -jar target/Shepherd-1.33.7.jar

.PHONY: clean
clean:
	@rm -rf target
	@mvn clean

.PHONY: pull
pull:
	@git pull origin --rebase

.PHONY: push
push:
	@git push origin

.PHONY: push_mirror
push_mirror:
	@! git config remote.mirror.url > /dev/null && git remote add mirror git@github.engineering.zhaw.ch:mazzoema/Shepherd.git
	@git push mirror
	@git remote remove mirror

.PHONY: bootstrap
bootstrap:
	@cp .env.example .env
