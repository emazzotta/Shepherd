package com.shepherd.zhaw.config;

import com.shepherd.zhaw.constraints.Constraint;

import java.util.ArrayList;
import java.util.List;

public class ConfigData {

	public static final String SHIFT_EARLY = "Fruehschicht";
	public static final String SHIFT_LATE = "Spaetschicht";
	public static final String SHIFT_NIGHT = "Nachtschicht";
	public static final char SHIFT_EARLY_SHORT = 'F';
	public static final char SHIFT_LATE_SHORT = 'S';
	public static final char SHIFT_NIGHT_SHORT = 'N';

	private ShiftType[] shiftTypes;
	private List<Employee> employees;
	private List<Constraint> constraints;
	private int workingHoursPerDay;
	private String plannedMonthsFolder;
	private boolean debugMode;

	public void initDefaultValues() {
		shiftTypes = new ShiftType[3];
		shiftTypes[0] = new ShiftType(SHIFT_EARLY, 300);
		shiftTypes[1] = new ShiftType(SHIFT_LATE, 200);
		shiftTypes[2] = new ShiftType(SHIFT_NIGHT, 100);
		constraints = new ArrayList<>();
		employees = new ArrayList<>();
		workingHoursPerDay = 8;
		plannedMonthsFolder = "Schichtplaene";
		debugMode = false;
	}

	public ShiftType[] getShiftTypes() {
		return shiftTypes;
	}

	public void setConstraints(List<Constraint> constraints) {
		this.constraints = constraints;
	}

	public List<Constraint> getConstraints() {
		return constraints;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public int getWorkingHoursPerDay() {
		return workingHoursPerDay;
	}

	public String getPlannedMonthsFolder() {
		return plannedMonthsFolder;
	}

	public boolean isDebugMode() {
		return debugMode;
	}
}
