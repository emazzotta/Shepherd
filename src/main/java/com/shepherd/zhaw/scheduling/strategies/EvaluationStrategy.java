package com.shepherd.zhaw.scheduling.strategies;

import com.shepherd.zhaw.scheduling.PlannedMonth;
import com.shepherd.zhaw.scheduling.TemporaryAssignment;

import java.util.Comparator;

public abstract class EvaluationStrategy implements Comparator<TemporaryAssignment> {
	protected PlannedMonth plannedMonth;
	
	/**
	 * Setting the PlannedMonth on which the calculations are based off.
	 * @param the PlannedMonth on which the calculations are based off
	 */
	public void setPlannedMonth(PlannedMonth plannedMonth) {
		this.plannedMonth = plannedMonth;
	}
	
	/**
	 * Returns the name of the strategy
	 * @return the name of the strategy
	 */
	public abstract String getName();
	
	/**
	 * Compares two TemporaryAssignment-Object based on the given strategy
	 * @param the first TemporaryAssignment-Object to compare
	 * @param the second TemporaryAssignment-Object to compare
	 */
	public abstract int compare(TemporaryAssignment ta1, TemporaryAssignment ta2);
}
