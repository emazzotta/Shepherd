package com.shepherd.zhaw.config;

import com.shepherd.zhaw.constraints.ConstraintFactory;
import com.shepherd.zhaw.constraints.ConstraintInput;
import com.shepherd.zhaw.constraints.mandatory.MandatoryConstraint;
import com.shepherd.zhaw.constraints.weighted.WeightedConstraint;
import com.shepherd.zhaw.scheduling.PlannedMonth;

import java.util.Date;
import java.util.List;

public class Employee {

	private String firstname, lastname;
	private int worktimePercent;
	private double worktimeBalance;

	public Employee() {
		this.worktimeBalance = 0;
		this.worktimePercent = 100;
	}

	public Employee(String firstname, String lastname, int workingtimePercent) {
		this();
		this.firstname = firstname;
		this.lastname = lastname;
		this.worktimePercent = workingtimePercent;
	}

	public boolean isValid(PlannedMonth plannedMonth, Date date, ShiftType shiftType) {
		ShiftType[] shiftTypes = Config.getData().getShiftTypes();
		ConstraintInput input = new ConstraintInput(plannedMonth, date, shiftType, this, shiftTypes);

		List<MandatoryConstraint> constraints = ConstraintFactory.getMandatory();

		return constraints.stream().noneMatch(c -> !c.isValid(input));
	}

	public int getPlanWeight(PlannedMonth plannedMonth, Date date, ShiftType shiftType) {
		ConstraintInput input = new ConstraintInput(plannedMonth, date, shiftType, this,
				Config.getData().getShiftTypes());
		List<WeightedConstraint> weighted = ConstraintFactory.getWeighted();
		return weighted.stream().map(r -> r.rate(input)).mapToInt(i -> i.intValue()).sum();
	}

	public void setWorktimeBalance(double worktimeBalance) {
		this.worktimeBalance = worktimeBalance;
	}

	public double getWorktimeBalance() {
		return worktimeBalance;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setWorktimePercent(int worktimePercent) {
		this.worktimePercent = worktimePercent;
	}

	public int getWorktimePercent() {
		return worktimePercent;
	}

	public String toString() {
		return firstname + " " + lastname + " (" + worktimePercent + "%)";
	}
}
