package com.shepherd.zhaw.constraints;

import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.config.ShiftType;
import com.shepherd.zhaw.scheduling.PlannedMonth;

import java.util.Date;

/**
 * Defines which objects constraints need to calculate weight or validation.
 */
public class ConstraintInput {

	/**
	 * Constructor for constraint input
	 * @param plannedMonth the month planned until current processing date
	 * @param date the current processing date
	 * @param shiftType the shift to be planned
	 * @param employee the employee to be planned
	 * @param shiftTypes list of possible shift types
	 */
	public ConstraintInput(PlannedMonth plannedMonth, Date date, ShiftType shiftType, Employee employee,
			ShiftType[] shiftTypes) {
		this.plannedMonth = plannedMonth;
		this.date = date;
		this.shiftType = shiftType;
		this.employee = employee;
		this.shiftTypes = shiftTypes;
	}

	/**
	 * Get month planned so far
	 * @return the planned month
	 */
	public PlannedMonth getPlannedMonth() {
		return plannedMonth;
	}

	/**
	 * Get date to be planned
	 * @return date to be planned
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Get shift type to be planned
	 * @return shift type to be planned
	 */
	public ShiftType getShiftType() {
		return shiftType;
	}

	/**
	 * Get employee to be planned
	 * @return employee to be planned
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Get shift types possible to plan
	 * @return shift types possible to plan
	 */
	public ShiftType[] getShiftTypes() {
		return shiftTypes;
	}

	private PlannedMonth plannedMonth;
	private Date date;
	private ShiftType shiftType;
	private Employee employee;
	private ShiftType[] shiftTypes;

}
