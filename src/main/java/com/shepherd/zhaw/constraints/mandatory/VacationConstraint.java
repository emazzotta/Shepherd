package com.shepherd.zhaw.constraints.mandatory;

import com.shepherd.zhaw.constraints.ConstraintInput;

import java.util.Calendar;

/**
 * Constraint for defining vacations of employees.
 */
public class VacationConstraint extends MandatoryConstraint {

    /**
     * Returns true if the shift to be planned conforms with this constraint
     * @param input contains information about all shifts planned so far and the current shift to be planned
     * @return if shift to be planned is conform with this constraint
     */
    @Override
    public boolean isValid(ConstraintInput input) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(input.getDate());

        if(input.getEmployee().getFirstname().equals(getSetting("employee", String.class))) {
            if(calendar.get(Calendar.DATE) == getSetting("day", Double.class).intValue()
                    && calendar.get(Calendar.MONTH) == getSetting("month", Double.class).intValue()
                    && calendar.get(Calendar.YEAR) == getSetting("year", Double.class).intValue()) {
                return false;
            }
        }

        return true;
    }
}
