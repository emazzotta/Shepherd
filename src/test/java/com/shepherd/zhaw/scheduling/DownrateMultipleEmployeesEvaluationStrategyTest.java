package com.shepherd.zhaw.scheduling;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.config.ShiftType;
import com.shepherd.zhaw.scheduling.strategies.DownrateMultipleEmployeesEvaluationStrategy;

public class DownrateMultipleEmployeesEvaluationStrategyTest {

	private ShiftType shiftType;
	private DownrateMultipleEmployeesEvaluationStrategy strategy;
	private Employee downratedEmployee1, downratedEmployee2;
	private ArrayList<Employee> downratedEmployees;
	private Employee notDownratedEmployee;

	@Before
	public void setUp() throws Exception {
		PlannedMonth plannedMonth = new PlannedMonth("Test");
		shiftType = Config.getData().getShiftTypes()[0];
		downratedEmployee1 = new Employee("X1", "X1", 100);
		downratedEmployee2 = new Employee("X2", "X2", 100);
		notDownratedEmployee = new Employee("A", "A", 100);
		
		downratedEmployees = new ArrayList<>();
		downratedEmployees.add(downratedEmployee1);
		downratedEmployees.add(downratedEmployee2);
		strategy = new DownrateMultipleEmployeesEvaluationStrategy(downratedEmployees);
		strategy.setPlannedMonth(plannedMonth);
	}

	@Test
	public void shouldCompareAccordingToAssignementWeight() {
		Employee employee = new Employee("A", "A", 100);
		TemporaryAssignment ta1 = new TemporaryAssignment(employee, shiftType, 1);
		TemporaryAssignment ta2 = new TemporaryAssignment(employee, shiftType, 0);
		assertEquals(strategy.compare(ta1, ta2), 1);
		assertEquals(strategy.compare(ta2, ta1), -1);
	}
	
	@Test
	public void shouldCompareAccordingToWorktimeBalance() {
		Employee employee1 = new Employee("A", "A", 100);
		employee1.setWorktimeBalance(1.0);
		Employee employee2 = new Employee("B", "B", 100);
		employee2.setWorktimeBalance(0.0);
		
		TemporaryAssignment ta1 = new TemporaryAssignment(employee1, shiftType, 0);
		TemporaryAssignment ta2 = new TemporaryAssignment(employee2, shiftType, 0);
		assertEquals(strategy.compare(ta1, ta2), 1);
		assertEquals(strategy.compare(ta2, ta1), -1);
	}
	
	@Test
	public void shouldCompareAccordingToPLannedMonthWorktimeBalance() {
		Employee employee1 = new Employee("A", "A", 100);
		Employee employee2 = new Employee("B", "B", 100);
		PlannedMonth plannedMonth = new PlannedMonth("Test");
		DownrateMultipleEmployeesEvaluationStrategy strategy = new DownrateMultipleEmployeesEvaluationStrategy(new ArrayList<Employee>());
		strategy.setPlannedMonth(plannedMonth);
		PlannedDay plannedDay = new PlannedDay(new Date(0));
		plannedDay.addPlannedEmployee(shiftType, employee1);
		plannedMonth.addPlannedDay(new Date(0), plannedDay);
		
		TemporaryAssignment ta1 = new TemporaryAssignment(employee1, shiftType, 0);
		TemporaryAssignment ta2 = new TemporaryAssignment(employee2, shiftType, 0);
		assertEquals(strategy.compare(ta1, ta2), 1);
		assertEquals(strategy.compare(ta2, ta1), -1);
	}
	
	@Test
	public void shouldCompareAccordingToDownratedEmployee() {
		TemporaryAssignment ta1 = new TemporaryAssignment(downratedEmployee1, shiftType, 0);
		TemporaryAssignment ta2 = new TemporaryAssignment(downratedEmployee1, shiftType, 0);
		TemporaryAssignment ta3 = new TemporaryAssignment(notDownratedEmployee, shiftType, 0);
		assertEquals(strategy.compare(ta1, ta2), -1);
		assertEquals(strategy.compare(ta2, ta1), -1);
		assertEquals(strategy.compare(ta1, ta3), 1);
		assertEquals(strategy.compare(ta3, ta1), -1);
		assertEquals(strategy.compare(ta2, ta3), 1);
		assertEquals(strategy.compare(ta3, ta2), -1);
	}
	
	@Test
	public void shouldCompareAccordingToDownratedEmployeeDuringOneWeek() {
		PlannedMonth plannedMonth = new PlannedMonth("Test");
		DownrateMultipleEmployeesEvaluationStrategy strategy = new DownrateMultipleEmployeesEvaluationStrategy(downratedEmployees);
		strategy.setPlannedMonth(plannedMonth);
		int dayIndex = 0;
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		
		TemporaryAssignment ta1 = new TemporaryAssignment(downratedEmployee1, shiftType, 0);
		TemporaryAssignment ta2 = new TemporaryAssignment(downratedEmployee1, shiftType, 0);
		TemporaryAssignment ta3 = new TemporaryAssignment(notDownratedEmployee, shiftType, 0);
		assertEquals(strategy.compare(ta1, ta2), -1);
		assertEquals(strategy.compare(ta2, ta1), -1);
		assertEquals(strategy.compare(ta1, ta3), 1);
		assertEquals(strategy.compare(ta3, ta1), -1);
		assertEquals(strategy.compare(ta2, ta3), 1);
		assertEquals(strategy.compare(ta3, ta2), -1);
	}
	
	@Test
	public void shouldCompareAccordingToDownratedEmployeeDuringAfterWeek() {
		PlannedMonth plannedMonth = new PlannedMonth("Test");
		DownrateMultipleEmployeesEvaluationStrategy strategy = new DownrateMultipleEmployeesEvaluationStrategy(downratedEmployees);
		strategy.setPlannedMonth(plannedMonth);
		int dayIndex = 0;
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		plannedMonth.addPlannedDay(new Date(24*60*60*100*dayIndex++), new PlannedDay(new Date(0)));
		
		TemporaryAssignment ta1 = new TemporaryAssignment(downratedEmployee1, shiftType, 0);
		TemporaryAssignment ta2 = new TemporaryAssignment(downratedEmployee1, shiftType, 0);
		TemporaryAssignment ta3 = new TemporaryAssignment(notDownratedEmployee, shiftType, 0);
		assertEquals(strategy.compare(ta1, ta2), -1);
		assertEquals(strategy.compare(ta2, ta1), -1);
		assertEquals(strategy.compare(ta1, ta3), -1);
		assertEquals(strategy.compare(ta3, ta1), -1);
		assertEquals(strategy.compare(ta2, ta3), -1);
		assertEquals(strategy.compare(ta3, ta2), -1);
	}
	
	@Test
	public void shouldCompareIfEqual() {
		Employee employee = new Employee("A", "A", 100);
		TemporaryAssignment ta1 = new TemporaryAssignment(employee, shiftType, 1);
		assertEquals(strategy.compare(ta1, ta1), 0);
	}
}
