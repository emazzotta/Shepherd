package com.shepherd.zhaw.constraints;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.constraints.mandatory.MandatoryConstraint;
import com.shepherd.zhaw.constraints.weighted.WeightedConstraint;
import com.shepherd.zhaw.ui.helper.PackageReader;
import com.shepherd.zhaw.ui.model.ConstraintTypeModel;

/**
 * Factory that instantiates constraints and collects Constraint classes
 * dynamically.
 */
public class ConstraintFactory {

	/**
	 * Get all constraints from configuration - each constraint instantiated as actual type specified
	 * @return constraints from configuration
	 */
	private static List<Constraint> getAllConstraints() {
		return Config.getData().getConstraints().stream().map(c -> {
			try {
				Class<?> clazz = Class.forName(c.getConstraintType());
				Constructor<?> ctor = clazz.getConstructor();
				Constraint object = (Constraint) ctor.newInstance();
				object.setName(c.getName());
				object.setConstraintType(c.getConstraintType());
				object.setSettings(c.getSettings());
				return object;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}).collect(Collectors.toList());
	}

	/**
	 * Filter constraints based on type
	 * @param type the type to filter
	 * @param <T>
	 * @return filtered list of constraints
	 */
	private static <T> List<T> getFiltered(Class<T> type) {
		// Map to specific Constraint types
		return getAllConstraints().stream().filter(c -> type.isInstance(c)).map(c -> type.cast(c))
				.collect(Collectors.toList());
	}

	private static List<WeightedConstraint> weightedConstraints;

	/**
	 * Return all weighted constraints configured (constraints inheriting from WeightedConstraint)
	 * @return weighted constraints
	 */
	public static List<WeightedConstraint> getWeighted() {
		if (weightedConstraints == null)
			weightedConstraints = getFiltered(WeightedConstraint.class);
		return weightedConstraints;
	}

	private static List<MandatoryConstraint> mandatoryConstraints;

	/**
	 * Return all mandatory constraints configured (constraints inheriting from MandatoryConstraint)
	 * @return mandatory constraints
	 */
	public static List<MandatoryConstraint> getMandatory() {
		if (mandatoryConstraints == null)
			mandatoryConstraints = getFiltered(MandatoryConstraint.class);
		return mandatoryConstraints;
	}

	/**
	 * Build list with all available constraint types in relevant packages
	 * @return list of all available constraints
	 */
	public static List<ConstraintTypeModel> getConstraintTypes() {
		List<ConstraintTypeModel> constraintTypes = new ArrayList<>();
		try {
			Class<?>[] clazzes = ArrayUtils.addAll( //
					PackageReader.getClasses("com.shepherd.zhaw.constraints.mandatory"), //
					PackageReader.getClasses("com.shepherd.zhaw.constraints.weighted"));

			for (Class<?> clazz : clazzes) {
				if (clazz != Constraint.class) {
					constraintTypes.add(new ConstraintTypeModel(clazz));
				}
			}

		} catch (Exception ex) {
		}

		return constraintTypes;
	}

}
