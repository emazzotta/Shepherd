package com.shepherd.zhaw.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Config {

	private static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private final String configFileName = "config.json";

	private static Config instance;
	private ConfigData data;
	private Gson gson;

	private Config() {
		GsonBuilder builder = new GsonBuilder();
		gson = builder.create();
		readConfigFile();
	}

	public static ConfigData getData() {
		if (instance == null) {
			instance = new Config();
		}
		return instance.data;
	}

	public static void setDataForTests(ConfigData configData) {
		if (instance == null) {
			instance = new Config();
		}
		instance.data = configData;
	}

	public static void storeData() {
		if (instance == null) {
			instance = new Config();
		}
		instance.writeConfigFile();
	}

	private void writeConfigFile() {
		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(configFileName), "utf-8"));
			writer.write(gson.toJson(data));
		} catch (IOException ex) {
			// TODO: write to error.log
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (Exception e) {
			    e.printStackTrace();
			}
		}
	}

	private void readConfigFile() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(configFileName), "UTF8"));
			StringBuilder json = new StringBuilder();
			String jsonLine;
			while ((jsonLine = reader.readLine()) != null) {
				json.append(jsonLine);
			}
			data = gson.fromJson(json.toString(), ConfigData.class);
			if (data == null) {
				throw new IOException();
			}
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, "Could not read config file", ex);
			data = new ConfigData();
			data.initDefaultValues();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (Exception e) {
                e.printStackTrace();
			}
		}
	}
}
