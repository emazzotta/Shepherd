package com.shepherd.zhaw.scheduling;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Stack;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.export.PlannedMonthExport;
import com.shepherd.zhaw.scheduling.exceptions.SchedulingCouldNotBeFinishedException;
import com.shepherd.zhaw.scheduling.strategies.BestEffortEvaluationStrategy;
import com.shepherd.zhaw.scheduling.strategies.DownrateMultipleEmployeesEvaluationStrategy;
import com.shepherd.zhaw.scheduling.strategies.DownrateSingleEmployeeEvaluationStrategy;
import com.shepherd.zhaw.scheduling.strategies.EvaluationStrategy;
import com.shepherd.zhaw.scheduling.strategies.IgnoreTransferredWorktimeEvaluationStrategy;

import javafx.concurrent.Task;

/**
 * Scheduler loops through all Strategies and calculates different Planned
 * Months based on those Strategies.
 */
public class Scheduler extends Task<PlannedMonth> {

	private TreeSet<PlannedMonth> plannedMonths;
	private int month, year;
	private String plannedMonthName;
	private final static Logger LOGGER = Logger.getLogger("scheduler");
	private ResourceBundle resources;

	public Scheduler(int month, int year, String plannedMonthName, ResourceBundle resources) {
		this.month = month;
		this.year = year;
		this.plannedMonthName = plannedMonthName;
		this.resources = resources;
	}

	/**
	 * This Function enables Scheduler to be used as JavaFX Task
	 */
	@Override
	protected PlannedMonth call() throws Exception {
		Date startDate = getFirstDateOfMonth(month, year);
		Date endDate = getLastDateOfMonth(month, year);
		return scheduleMonth(startDate, endDate, plannedMonthName);
	}

	/**
	 * Loops through all Strategies and calculates different PlannedMonths based
	 * on those Strategies.
	 * 
	 * @param Date
	 *            on which the calculation should start
	 * @param Date
	 *            on which the calculation should end
	 * @param The
	 *            name of the plan
	 * @return The best calculated PlannedMonth
	 */
	public PlannedMonth scheduleMonth(Date startDate, Date endDate, String plannedMonthName) {
		plannedMonths = new TreeSet<PlannedMonth>();

		try {
			SchedulerMonth monthScheduler = new SchedulerMonth(startDate, endDate, plannedMonthName);

			Stack<EvaluationStrategy> evaluationStrategies = getEvaluationStrategyQueue();
			int amountOfStrategies = evaluationStrategies.size();

			while (!evaluationStrategies.isEmpty() && !isCancelled()) {
				EvaluationStrategy evaluationStrategy = evaluationStrategies.pop();
				updateProgress((evaluationStrategies.size() - amountOfStrategies) + 1, amountOfStrategies);
				updateMessage(MessageFormat.format(resources.getString("label.progress.calculateWithStrategy"),
						evaluationStrategy.getName()));
				PlannedMonth plannedMonth = null;
				try {
					plannedMonth = monthScheduler.schedule(evaluationStrategy);
					plannedMonths.add(plannedMonth);
					if (Config.getData().isDebugMode()) {
						PlannedMonthExport.export(plannedMonth, "debug");
					}
				} catch (SchedulingCouldNotBeFinishedException ex) {
					LOGGER.log(Level.WARNING, "Could not find schedule solution with strategy {0}",
							evaluationStrategy.getClass().getName());
				}
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}

		return !plannedMonths.isEmpty() ? plannedMonths.first() : null;
	}

	/**
	 * Returns a Stack of Strategies to be used
	 * 
	 * @return Stack of Strategies
	 */
	private Stack<EvaluationStrategy> getEvaluationStrategyQueue() {
		Stack<EvaluationStrategy> evaluationStrategies = new Stack<EvaluationStrategy>();
		for (Employee firstEmployee : Config.getData().getEmployees()) {
			for (Employee secondEmployee : Config.getData().getEmployees()) {
				ArrayList<Employee> employees = new ArrayList<Employee>(2);
				employees.add(firstEmployee);
				employees.add(secondEmployee);
				evaluationStrategies.push(new DownrateMultipleEmployeesEvaluationStrategy(employees));
			}
		}
		for (Employee employee : Config.getData().getEmployees()) {
			evaluationStrategies.push(new DownrateSingleEmployeeEvaluationStrategy(employee));
		}
		evaluationStrategies.push(new IgnoreTransferredWorktimeEvaluationStrategy());
		evaluationStrategies.push(new BestEffortEvaluationStrategy());
		return evaluationStrategies;
	}

	/**
	 * The First Date of the given month and year
	 * 
	 * @param the
	 *            given month
	 * @param the
	 *            given year
	 * @return First Date of the given month and year
	 */
	private Date getFirstDateOfMonth(int month, int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, 1, 0, 0, 0);
		return calendar.getTime();
	}

	/**
	 * The Last Date of the given month and year
	 * 
	 * @param the
	 *            given month
	 * @param the
	 *            given year
	 * @return Last Date of the given month and year
	 */
	private Date getLastDateOfMonth(int month, int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, 1, 0, 0, 0);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return calendar.getTime();
	}
}
