package com.shepherd.zhaw.ui;

import com.shepherd.zhaw.Shepherd;
import javafx.fxml.FXMLLoader;

import java.io.IOException;
import java.util.PropertyResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXMLLoader with sheperd-properties resource bundle
 */
public class ShepherdFXMLLoader extends FXMLLoader {

	private static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public ShepherdFXMLLoader(String uri) {
		setLocation(Shepherd.class.getResource(uri));
		try {
			setResources(new PropertyResourceBundle(Shepherd.class.getResourceAsStream("/shepherd_de.properties")));
		} catch (IOException e) {
			LOGGER.log(Level.FINE, "Can't define properties", e);
		}
	}
}
