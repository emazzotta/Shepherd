package com.shepherd.zhaw.ui.dialog;

import javafx.scene.control.Alert;

/**
 * Warning Dialog. Shows a warning icon with a specific message
 */
public class WarningDialog extends Alert {

	public WarningDialog(String title, String message) {
		super(AlertType.WARNING);
		setHeaderText(title);
		setContentText(message);
	}

}
