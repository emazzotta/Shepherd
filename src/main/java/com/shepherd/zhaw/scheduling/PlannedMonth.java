package com.shepherd.zhaw.scheduling;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.Employee;

import java.util.*;

/**
 * Compostion of all planned days 
 */
public class PlannedMonth implements Comparable<PlannedMonth> {
	private HashMap<Date, PlannedDay> plannedDays;
	private HashMap<Employee, ArrayList<PlannedDay>> plannedDaysByEmployee;
	private HashMap<Employee, Integer> employeeWorktimes;
	private int weight = 0;
	private String name;
	
	public PlannedMonth(String name) {
		this.plannedDays = new HashMap<>(31);
		this.plannedDaysByEmployee = new HashMap<>();
		this.employeeWorktimes = new HashMap<>();
		this.name = name;
	}
	
	/**
	 * Add a planned day to the planned month			
	 * @param the date of the planned month
	 * @param the planned day to add
	 */
	public void addPlannedDay(Date date, PlannedDay plannedDay) {
		plannedDays.put(date, plannedDay);
		for (Employee employee : plannedDay.getAllPlannedEmployees()) {
			addPlannedDayByEmployee(employee, plannedDay);
			addWorktime(employee);
		}
		addWeight(plannedDay.getWeight());
	}

	/**
	 * Returns all employee planned in any day
	 * @return all employee planned in any day
	 */
	public Set<Employee> getAllEmployees() {
        Set<Employee> allEmployees = new HashSet<>();
        for (Date currentDate : plannedDays.keySet()) {
            allEmployees.addAll(plannedDays.get(currentDate).getAllPlannedEmployees());
        }
        return allEmployees;
    }
	
	/**
	 * Returns all planned day which involve the specific employee
	 * @param the employee of which all planned days should be returned
	 * @return all planned day which involve the given employee
	 */
	public ArrayList<PlannedDay> getPlannedDaysByEmployee(Employee employee) {
		ArrayList<PlannedDay> plannedDaysByEmplyee;
		if (plannedDaysByEmployee.containsKey(employee)) {
			plannedDaysByEmplyee = plannedDaysByEmployee.get(employee); 
		} else {
			plannedDaysByEmplyee = new ArrayList<PlannedDay>(); 
		}
		return plannedDaysByEmplyee;
	}

	/**
	 * Return all planned days
	 * @return all planned days
	 */
    public HashMap<Date, PlannedDay> getPlannedDays() {
        return plannedDays;
    }

    public double getWorktimeBalance(Employee employee) {
    	double expectedWorktime = (Config.getData().getWorkingHoursPerDay() * (employee.getWorktimePercent()/100.0) * (5.0/7.0) * plannedDays.size());
    	double actualWorktime = employeeWorktimes.getOrDefault(employee, 0);
		return actualWorktime - expectedWorktime; 
	}
    
    /**
     * Return the cumulated weight of the planned month
     * @return the cumulated weight of the planned month
     */
	private int getWeight() {
		return weight;
	}

	/**
	 * Add weight to the planned month
	 * @param the weight to add
	 */
	private void addWeight(int additionalWeight) {
		this.weight += additionalWeight;
	}

	/**
	 * Adds a planned day to the planned month by employee 
	 * @param the employee on which context the planned day should be added
	 * @param the planned day to add
	 */
	private void addPlannedDayByEmployee(Employee employee, PlannedDay plannedDay) {
		if (plannedDaysByEmployee.containsKey(employee)) {
			ArrayList<PlannedDay> plannedDays = plannedDaysByEmployee.get(employee);
			plannedDays.add(plannedDay);
			plannedDaysByEmployee.put(employee, plannedDays);
		} else {
			ArrayList<PlannedDay> plannedDays = new ArrayList<>();
			plannedDays.add(plannedDay);
			plannedDaysByEmployee.put(employee, plannedDays);
		}
	}
	
	/**
	 * Adds worktime to a certain employee to calculate work time balance
	 * @param the employee on which the worktime should be added
	 */
	private void addWorktime(Employee employee) {
		int workingHoursPerDay = Config.getData().getWorkingHoursPerDay();
		if (employeeWorktimes.containsKey(employee)) {
			int worktime = employeeWorktimes.get(employee);
			worktime += workingHoursPerDay;
			employeeWorktimes.put(employee, worktime);
		} else {
			employeeWorktimes.put(employee, workingHoursPerDay);
		}
	}

	/**
	 * implementation of the Comparable method to allow the plannedmonth to be sorted by weight
	 * @param the planned month this planned month should be compared to
	 */
	@Override
	public int compareTo(PlannedMonth otherPlannedMonth) {
		int weightDifference = getWeight() - otherPlannedMonth.getWeight();
		if (weightDifference == 0 && !this.equals(otherPlannedMonth)) {
			weightDifference = -1;
		}
		return weightDifference;
	}

	/**
	 * Returns the name of the current plannen month
	 * @return the name of the current plannen month
	 */
	public String getName() {
		return name;
	}
}
