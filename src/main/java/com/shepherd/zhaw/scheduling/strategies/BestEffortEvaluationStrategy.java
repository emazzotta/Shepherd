package com.shepherd.zhaw.scheduling.strategies;

import com.shepherd.zhaw.scheduling.TemporaryAssignment;

import java.io.Serializable;

/**
 *	Evaluation strategy which follows the best effort
 */
public class BestEffortEvaluationStrategy extends EvaluationStrategy implements Serializable {

    private static final long serialVersionUID = 1;
	
	/**
	 * Compares two TemporaryAssignment-Object based on the best effort
	 * @param the first TemporaryAssignment-Object to compare
	 * @param the second TemporaryAssignment-Object to compare
	 */
	@Override
	public int compare(TemporaryAssignment ta1, TemporaryAssignment ta2) {
		double ta1WorktimeBalance = plannedMonth.getWorktimeBalance(ta1.getEmployee()) + ta1.getEmployee().getWorktimeBalance();
		double ta2WorktimeBalance = plannedMonth.getWorktimeBalance(ta2.getEmployee()) + ta2.getEmployee().getWorktimeBalance();  
		
		if (Double.compare(ta1.getAssignementWeight() + ta1WorktimeBalance, ta2.getAssignementWeight() + ta2WorktimeBalance) == 1) {
			return 1;
		} else if (Double.compare(ta1.getAssignementWeight() + ta1WorktimeBalance, ta2.getAssignementWeight() + ta2WorktimeBalance) == -1 || !ta1.equals(ta2)) {
			return -1;
		} else {
			return 0;
		}
	}

	/**
	 * Returns the name of the strategy
	 * @return the name of the strategy
	 */
	@Override
	public String getName() {
		return "Best Effort";
	}
}
