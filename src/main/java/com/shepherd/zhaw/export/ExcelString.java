package com.shepherd.zhaw.export;

import org.apache.poi.ss.usermodel.IndexedColors;

public class ExcelString {

	private String text;
	private IndexedColors color;
	private short fontHeightInPoints;
	private short rotation;

	public ExcelString(String text) {
		this(text, IndexedColors.AUTOMATIC);
	}

	public ExcelString(String text, IndexedColors color) {
		this(text, color, 0);
	}

	public ExcelString(char character, IndexedColors color) {
		this(String.valueOf(character), color, 0);
	}

	public ExcelString(String text, IndexedColors color, int rotation) {
		this(text, color, rotation, 11);
	}

	public ExcelString(String text, IndexedColors color, int rotation, int fontHeightInPoints) {
		this.text = text;
		this.color = color;
		this.rotation = (short) rotation;
		this.fontHeightInPoints = (short) fontHeightInPoints;
	}

	public String getText() {
		return text;
	}

	public IndexedColors getColor() {
		return color;
	}

	public short getRotation() {
		return rotation;
	}

	public short getFontHeightInPoints() {
		return fontHeightInPoints;
	}
}
