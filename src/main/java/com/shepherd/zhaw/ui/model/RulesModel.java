package com.shepherd.zhaw.ui.model;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.shepherd.zhaw.constraints.Constraint;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Model for Rules-Screen
 */
public class RulesModel {

	private static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private final SimpleStringProperty name;
	private final SimpleObjectProperty<ConstraintTypeModel> constraintType;
	private final SimpleStringProperty settings;

	public RulesModel() {
		this.name = new SimpleStringProperty();
		this.constraintType = new SimpleObjectProperty<>();
		this.settings = new SimpleStringProperty();
	}

	@SuppressWarnings("unchecked")
	public RulesModel(Constraint constraint) {
		this.name = new SimpleStringProperty(constraint.getName());
		Class<? extends Constraint> clazz = null;
		try {
			clazz = (Class<? extends Constraint>) Class.forName(constraint.getConstraintType());
		} catch (ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Class not found!", ex);
		}
		if (clazz != null) {
			this.constraintType = new SimpleObjectProperty<>(new ConstraintTypeModel(clazz));
		} else {
			this.constraintType = new SimpleObjectProperty<>();
		}
		this.settings = new SimpleStringProperty(constraint.getSettings());
	}

	// methods
	public Constraint getJavaConstraint() {
		Constraint constraint = new Constraint();
		constraint.setName(name.get());
		constraint.setConstraintType(constraintType.get().getFull());
		constraint.setSettings(settings.get());
		return constraint;
	}

	public boolean isEmpty() {
		return getName() == null || getConstraintType() == null || getSettings() == null || //
				getName().isEmpty() || getSettings().isEmpty();
	}

	// getters and setters
	public void setName(String nameStr) {
		name.set(nameStr);
	}

	public String getName() {
		return name.get();
	}

	public SimpleStringProperty getNameProperty() {
		return name;
	}

	public void setConstraintType(ConstraintTypeModel contraintTypeModel) {
		constraintType.set(contraintTypeModel);
	}

	public ConstraintTypeModel getConstraintType() {
		return constraintType.get();
	}

	public SimpleObjectProperty<ConstraintTypeModel> getConstraintTypeProperty() {
		return constraintType;
	}

	public void setSettings(String settingsStr) {
		settings.set(settingsStr);
	}

	public String getSettings() {
		return settings.get();
	}

	public SimpleStringProperty getSettingsProperty() {
		return settings;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RulesModel [name=");
		builder.append(name);
		builder.append(", constraintType=");
		builder.append(constraintType);
		builder.append(", settings=");
		builder.append(settings);
		builder.append("]");
		return builder.toString();
	}
}
