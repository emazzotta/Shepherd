package com.shepherd.zhaw.ui;

import java.awt.Desktop;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.shepherd.zhaw.Shepherd;
import com.shepherd.zhaw.ui.helper.ShepherdScreen;

import javafx.fxml.FXML;

/**
 * Controller for RootLayout.fxml
 */
public class RootLayoutController {

	private static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	@FXML
	private void handleExit() {
		System.exit(0);
	}

	/**
	 * Read file from resources and open it as a file...
	 */
	@FXML
	private void openAGB() {
		try {
			Path tempOutput = null;
			String tempFile = "agb";
			tempOutput = Files.createTempFile(tempFile, ".pdf");
			tempOutput.toFile().deleteOnExit();
			InputStream is = getClass().getResourceAsStream(Shepherd.AGB_PATH);
			Files.copy(is, tempOutput, StandardCopyOption.REPLACE_EXISTING);
			if (Desktop.isDesktopSupported()) {
				Desktop dTop = Desktop.getDesktop();
				if (dTop.isSupported(Desktop.Action.OPEN)) {
					dTop.open(tempOutput.toFile());
				}
			}
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, "AGBs not found", ex);
		}
	}

	@FXML
	private void openAbout() {
		ShepherdScreen.openAboutScreen();
	}
}