package com.shepherd.zhaw.ui.helper;

import com.shepherd.zhaw.ui.EditRuleController;
import com.shepherd.zhaw.ui.ProgressScreenController;
import com.shepherd.zhaw.ui.ShepherdFXMLLoader;
import com.shepherd.zhaw.ui.ShepherdStage;
import com.shepherd.zhaw.ui.model.RulesModel;
import com.shepherd.zhaw.ui.model.ScheduleData;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper class to open different screens
 */
public class ShepherdScreen {

	private static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static final String ROOT_LAYOUT = "RootLayout.fxml";
	public static final String ABOUT_SCREEN = "AboutScreen.fxml";
	public static final String CREATE_NEW_SCHEDULE_SCREEN = "CreateNewScheduleScreen.fxml";
	public static final String DEFINE_RULES_SCREEN = "DefineRulesScreen.fxml";
	public static final String MANAGE_STAFF_SCREEN = "ManageStaffScreen.fxml";
	public static final String SHOW_PROGRESS_SCREEN = "ShowProgressScreen.fxml";
	public static final String EDIT_RULE_SCREEN = "EditRuleScreen.fxml";
	public static final String START_SCREEN = "StartScreen.fxml";

	public static void openAboutScreen() {
		ShepherdScreen.openScreen(ABOUT_SCREEN);
	}

	public static void openEditStaffScreen(Scene owner) {
		ShepherdScreen.openScreen(owner, MANAGE_STAFF_SCREEN);
	}

	public static void openProgressScreen(Scene owner, ScheduleData scheduleData) {
		try {
			Stage stage = new ShepherdStage(owner);
			FXMLLoader loader = ShepherdScreen.getLoader(SHOW_PROGRESS_SCREEN);
			loader.setController(new ProgressScreenController(scheduleData));
			stage.setScene(new Scene(loader.load()));
			stage.setResizable(false);
			stage.showAndWait();
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, "Could not open ProgressScreen", ex);
		}
	}

	public static void openEditRuleScreen(Scene owner, RulesModel rulesModel) {
		try {
			Stage stage = new ShepherdStage(owner);
			FXMLLoader loader = ShepherdScreen.getLoader(EDIT_RULE_SCREEN);
			loader.setController(new EditRuleController(rulesModel));
			stage.setScene(new Scene(loader.load()));
			stage.setResizable(false);
			stage.showAndWait();
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, "Could not open EditRuleScreen", ex);
		}
	}

	public static String getFXMLPath(String filename) {
		return "/ui/" + filename;
	}

	public static void openRulesScreen(Scene owner) {
		ShepherdScreen.openScreen(owner, DEFINE_RULES_SCREEN);
	}

	public static void openCreateScheduleScreen(Scene owner) {
		ShepherdScreen.openScreen(owner, CREATE_NEW_SCHEDULE_SCREEN);
	}

	public static FXMLLoader getLoader(String filename) {
		return new ShepherdFXMLLoader(getFXMLPath(filename));
	}

	private static void openScreen(String filename) {
		openScreen(null, filename);
	}

	private static void openScreen(Scene owner, String filename) {
		try {
			Stage stage = new ShepherdStage(owner);
			FXMLLoader loader = ShepherdScreen.getLoader(filename);
			stage.setScene(new Scene(loader.load()));
			stage.setResizable(false);
			stage.show();
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, "Could not open screen " + filename, ex);
		}
	}

}
