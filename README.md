[![build status](https://gitlab.com/emazzotta/Shepherd/badges/master/build.svg)](https://gitlab.com/emazzotta/Shepherd/pipelines)
[![coverage report](https://gitlab.com/emazzotta/Shepherd/badges/master/coverage.svg)](https://gitlab.com/emazzotta/Shepherd/commits/master)
[![Developed By](https://img.shields.io/badge/developed%20with%20♥%20by-Team%20Shepherd%20at%20ZHAW-blue.svg)](https://zhaw.ch/)

# Shepherd

A sophisticated piece of software, written in Java, to simplify and automate the allocation of work shifts in hospitals.

### Download 

https://gitlab.com/emazzotta/Shepherd/builds/artifacts/master/download?job=artifact

## Setup

* Install [Java JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Install [Maven 3](https://maven.apache.org/download.cgi)  
### For Windows user only:
* Install [Git for Windows](https://git-for-windows.github.io/) 
* Install [additional functionality](https://gist.github.com/evanwill/0207876c3243bbb6863e65ec5dc3f058) for Git for Windows

```bash
git clone git@gitlab.com:emazzotta/Shepherd.git
cd Shepherd
make bootstrap
```

## Quickstart

To just run the software

```bash
make artifact;make run
```

## Build

```bash
make build
```

## Tests

```bash
make test
```

## Artifact

```bash
make artifact
```

## Run

```bash
make run
```

## Note

**Repository https://github.engineering.zhaw.ch/mazzoema/Shepherd**  
Mirror Repository, "official" ZHAW Engineering Repository  
**Repository https://gitlab.com/emazzotta/Shepherd**  
Working repository to run CI, calculate test coverage and offer cool badges. 

```bash
# To update the ZHAW repository
make push_mirror
```

## Authors

Emanuele Mazzotta  
Martin Ewald  
Patrick Brunner  
Raphael Müller  
Sebastian Brunner  
