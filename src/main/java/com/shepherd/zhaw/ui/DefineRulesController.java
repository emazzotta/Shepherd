package com.shepherd.zhaw.ui;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.constraints.Constraint;
import com.shepherd.zhaw.ui.comparator.RulesModelComparator;
import com.shepherd.zhaw.ui.helper.ShepherdScreen;
import com.shepherd.zhaw.ui.model.ConstraintTypeModel;
import com.shepherd.zhaw.ui.model.RulesModel;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Controller for DefineRulesScreen.fxml
 */
public class DefineRulesController implements Initializable {

	private ObservableList<RulesModel> dataList;
	private RulesModel editRule;

	@FXML
	private AnchorPane anchorPane;

	@FXML
	private TableView<RulesModel> rulesTable;

	@FXML
	private TableColumn<RulesModel, String> nameColumn;

	@FXML
	private TableColumn<RulesModel, ConstraintTypeModel> constraintTypeColumn;

	@FXML
	private TableColumn<RulesModel, String> settingsColumn;

	@FXML
	private TableColumn<RulesModel, RulesModel> editColumn;

	@FXML
	private TableColumn<RulesModel, RulesModel> deleteColumn;

	public DefineRulesController() {
		this.dataList = FXCollections.observableArrayList();
		for (Constraint constraint : Config.getData().getConstraints()) {
			dataList.add(new RulesModel(constraint));
		}
		this.dataList.sort(new RulesModelComparator());
		this.editRule = new RulesModel();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		rulesTable.setItems(dataList);

		nameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		constraintTypeColumn.setCellValueFactory(cellData -> cellData.getValue().getConstraintTypeProperty());
		settingsColumn.setCellValueFactory(cellData -> cellData.getValue().getSettingsProperty());

		// edit
		editColumn.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		editColumn.setCellFactory(param -> new TableCell<RulesModel, RulesModel>() {

			private final Button editButton = new Button(resources.getString("action.edit"));

			@Override
			protected void updateItem(RulesModel rulesModel, boolean empty) {
				super.updateItem(rulesModel, empty);
				if (rulesModel == null) {
					setGraphic(null);
				} else {
					setGraphic(editButton);
					editButton.setOnAction(event -> editRule(rulesModel));
				}
			}
		});

		// delete
		deleteColumn.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		deleteColumn.setCellFactory(param -> new TableCell<RulesModel, RulesModel>() {

			private final Button deleteButton = new Button(resources.getString("action.delete"));

			@Override
			protected void updateItem(RulesModel rules, boolean empty) {
				super.updateItem(rules, empty);
				if (rules == null) {
					setGraphic(null);
				} else {
					setGraphic(deleteButton);
					deleteButton.setOnAction(event -> getTableView().getItems().remove(rules));
				}
			}
		});
	}

	public void editRule(RulesModel rulesModel) {
		editRule = rulesModel;
		ShepherdScreen.openEditRuleScreen(anchorPane.getScene(), editRule);
		if (!editRule.isEmpty()) {
			editRule = new RulesModel();
		}
	}

	@FXML
	public void addNewRule() {
		ShepherdScreen.openEditRuleScreen(anchorPane.getScene(), editRule);
		if (!editRule.isEmpty()) {
			dataList.add(editRule);
			editRule = new RulesModel();
		}
	}

	@FXML
	public void saveRules() {
		List<Constraint> constraints = new ArrayList<>();
		dataList.forEach(c -> constraints.add(c.getJavaConstraint()));
		Config.getData().setConstraints(constraints);
		Config.storeData();
		// close window
		Stage stage = (Stage) rulesTable.getScene().getWindow();
		stage.close();
	}
}