package com.shepherd.zhaw.ui.comparator;

import java.io.Serializable;
import java.util.Comparator;

import com.shepherd.zhaw.ui.model.EmployeeModel;

public class EmployeeModelComparator implements Comparator<EmployeeModel>, Serializable {

    private static final long serialVersionUID = 1;

	@Override
	public int compare(EmployeeModel o1, EmployeeModel o2) {
		int c = o1.getLastname().compareTo(o2.getLastname());
		if (c == 0) {
			c = o1.getFirstname().compareTo(o2.getFirstname());
		}
		return c;
	}

}
