package com.shepherd.zhaw.export;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.scheduling.PlannedDay;
import com.shepherd.zhaw.scheduling.PlannedMonth;

import java.io.File;
import java.nio.file.Paths;

import static com.shepherd.zhaw.util.Util.createDirectory;

public class PlannedMonthExport {

	public static String export(PlannedMonth plannedMonth) {
		return export(plannedMonth, Config.getData().getPlannedMonthsFolder());
	}

	public static String export(PlannedMonth plannedMonth, String targetFolderName) {
		String currentlocation = Paths.get(".").toAbsolutePath().normalize().toString();
		String plannedMonthsDirectory = currentlocation + File.separator + targetFolderName;
		createDirectory(plannedMonthsDirectory);

		String prefix = getPrefix(plannedMonth);
		String filePath;
		int revision = 0;

		do {
			revision++;
			filePath = String.format("%s%s%s-%04d.xlsx", plannedMonthsDirectory, File.separator, prefix, revision);
		} while ((new File(filePath)).exists());

		ExcelExport.write(plannedMonth, filePath);
		return filePath;
	}

	private static String getPrefix(PlannedMonth plannedMonth) {
		PlannedDay firstplannedDay = (PlannedDay) plannedMonth.getPlannedDays().values().toArray()[0];
		return String.format("%tY-%tm", firstplannedDay.getDate(), firstplannedDay.getDate());
	}
}
