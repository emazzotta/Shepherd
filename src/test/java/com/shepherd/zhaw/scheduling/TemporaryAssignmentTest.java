package com.shepherd.zhaw.scheduling;

import static org.junit.Assert.*;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.config.ShiftType;
import com.shepherd.zhaw.scheduling.strategies.BestEffortEvaluationStrategy;

import java.util.Date;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

public class TemporaryAssignmentTest {

	private Employee employeeNegativeWorktimeBalance;
	private Employee employeeEvenWorktimeBalance;
	private Employee employeePositiveWorktimeBalance;
	private ShiftType shiftType;
	
	@Before
	public void setUp() throws Exception {
		employeeNegativeWorktimeBalance = new Employee();
		employeeNegativeWorktimeBalance.setWorktimeBalance(-1);
		employeeEvenWorktimeBalance = new Employee();
		employeeEvenWorktimeBalance.setWorktimeBalance(0);
		employeePositiveWorktimeBalance = new Employee();
		employeePositiveWorktimeBalance.setWorktimeBalance(1);
		shiftType = Config.getData().getShiftTypes()[0];
	}

	@Test
	public void shouldOrderItselfAccordingToAssignementWeight() {
		PlannedMonth plannedMonth = new PlannedMonth("Test");
		BestEffortEvaluationStrategy strategy = new BestEffortEvaluationStrategy();
		strategy.setPlannedMonth(plannedMonth);

		TemporaryAssignment taNegativeWeight = new TemporaryAssignment(employeeEvenWorktimeBalance, shiftType, -1); 
		TemporaryAssignment taEvenWeight = new TemporaryAssignment(employeeEvenWorktimeBalance, shiftType, 0);
		TemporaryAssignment taPositiveWeight = new TemporaryAssignment(employeeEvenWorktimeBalance, shiftType, 1);
		
		TreeSet<TemporaryAssignment> assignments = new TreeSet<TemporaryAssignment>(strategy);
		assignments.add(taNegativeWeight);
		assignments.add(taEvenWeight);
		assignments.add(taPositiveWeight);
		assertEquals(assignments.toArray()[0], taNegativeWeight);
		assertEquals(assignments.toArray()[1], taEvenWeight);
		assertEquals(assignments.toArray()[2], taPositiveWeight);
		
		assignments = new TreeSet<TemporaryAssignment>(strategy);
		assignments.add(taPositiveWeight);
		assignments.add(taEvenWeight);
		assignments.add(taNegativeWeight);
		assertEquals(assignments.toArray()[0], taNegativeWeight);
		assertEquals(assignments.toArray()[1], taEvenWeight);
		assertEquals(assignments.toArray()[2], taPositiveWeight);
	}
	
	@Test
	public void shouldOrderItselfAccordingToPlannedWorktimeBalance() {
		PlannedMonth plannedMonth = new PlannedMonth("Test");
		BestEffortEvaluationStrategy strategy = new BestEffortEvaluationStrategy();
		strategy.setPlannedMonth(plannedMonth);
		
		PlannedDay plannedDay = new PlannedDay(new Date());
		plannedDay.addPlannedEmployee(Config.getData().getShiftTypes()[0], employeeEvenWorktimeBalance);
		plannedDay.addPlannedEmployee(Config.getData().getShiftTypes()[0], employeePositiveWorktimeBalance);
		plannedMonth.addPlannedDay(new Date(0), plannedDay);
		
		plannedDay = new PlannedDay(new Date());
		plannedDay.addPlannedEmployee(Config.getData().getShiftTypes()[0], employeePositiveWorktimeBalance);
		plannedMonth.addPlannedDay(new Date(24*60*60*1000), plannedDay);

		
		TemporaryAssignment taNegativePlannedWorktimeBalance = new TemporaryAssignment(employeeNegativeWorktimeBalance, shiftType, 0); 
		TemporaryAssignment taEvenPlannedWorktimeBalance = new TemporaryAssignment(employeeEvenWorktimeBalance, shiftType, 0);
		TemporaryAssignment taPositivePlannedWorktimeBalance = new TemporaryAssignment(employeePositiveWorktimeBalance, shiftType, 0);
		
		TreeSet<TemporaryAssignment> assignments = new TreeSet<TemporaryAssignment>(strategy);
		assignments.add(taNegativePlannedWorktimeBalance);
		assignments.add(taEvenPlannedWorktimeBalance);
		assignments.add(taPositivePlannedWorktimeBalance);
		assertEquals(assignments.toArray()[0], taNegativePlannedWorktimeBalance);
		assertEquals(assignments.toArray()[1], taEvenPlannedWorktimeBalance);
		assertEquals(assignments.toArray()[2], taPositivePlannedWorktimeBalance);
		
		assignments = new TreeSet<TemporaryAssignment>(strategy);
		assignments.add(taPositivePlannedWorktimeBalance);
		assignments.add(taEvenPlannedWorktimeBalance);
		assignments.add(taNegativePlannedWorktimeBalance);
		assertEquals(assignments.toArray()[0], taNegativePlannedWorktimeBalance);
		assertEquals(assignments.toArray()[1], taEvenPlannedWorktimeBalance);
		assertEquals(assignments.toArray()[2], taPositivePlannedWorktimeBalance);
	}
	
	@Test
	public void shouldOrderItselfAccordingToWorktimeBalance() {
		PlannedMonth plannedMonth = new PlannedMonth("Test");
		BestEffortEvaluationStrategy strategy = new BestEffortEvaluationStrategy();
		strategy.setPlannedMonth(plannedMonth);
		
		TemporaryAssignment taNegativeWorktimeBalance = new TemporaryAssignment(employeeNegativeWorktimeBalance, shiftType, 0); 
		TemporaryAssignment taEvenWorktimeBalance = new TemporaryAssignment(employeeEvenWorktimeBalance, shiftType, 0);
		TemporaryAssignment taPositiveWorktimeBalance = new TemporaryAssignment(employeePositiveWorktimeBalance,  shiftType, 0);
		
		TreeSet<TemporaryAssignment> assignments = new TreeSet<TemporaryAssignment>(strategy);
		assignments.add(taNegativeWorktimeBalance);
		assignments.add(taEvenWorktimeBalance);
		assignments.add(taPositiveWorktimeBalance);
		assertEquals(assignments.toArray()[0], taNegativeWorktimeBalance);
		assertEquals(assignments.toArray()[1], taEvenWorktimeBalance);
		assertEquals(assignments.toArray()[2], taPositiveWorktimeBalance);
		
		assignments = new TreeSet<TemporaryAssignment>(strategy);
		assignments.add(taPositiveWorktimeBalance);
		assignments.add(taEvenWorktimeBalance);
		assignments.add(taNegativeWorktimeBalance);
		assertEquals(assignments.toArray()[0], taNegativeWorktimeBalance);
		assertEquals(assignments.toArray()[1], taEvenWorktimeBalance);
		assertEquals(assignments.toArray()[2], taPositiveWorktimeBalance);
	}
}
