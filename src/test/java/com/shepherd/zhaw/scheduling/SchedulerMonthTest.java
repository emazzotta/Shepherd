package com.shepherd.zhaw.scheduling;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.ConfigData;
import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.scheduling.exceptions.SchedulingCouldNotBeFinishedException;
import com.shepherd.zhaw.scheduling.strategies.BestEffortEvaluationStrategy;

public class SchedulerMonthTest {
	BestEffortEvaluationStrategy strategy; 
	
	@Before
	public void setUp() throws Exception {
		strategy = new BestEffortEvaluationStrategy();
	}

	@Test(expected=SchedulingCouldNotBeFinishedException.class)
	public void shouldThrowExceptionIfNotEngoughEmployees() throws SchedulingCouldNotBeFinishedException {
		ConfigData configData = new ConfigData();
		configData.initDefaultValues();
		Config.setDataForTests(configData);
		new SchedulerMonth(new Date(0), new Date(24*60*60*1000), "Test").schedule(strategy);
	}
	
	@Test
	public void shouldPlanTheCorrectNumberOfDays() throws SchedulingCouldNotBeFinishedException {
		ConfigData configData = new ConfigData();
		configData.initDefaultValues();
		
		ArrayList<Employee> employees = new ArrayList<>();
		employees.add(new Employee("A", "A", 100));
		employees.add(new Employee("B", "B", 100));
		employees.add(new Employee("C", "C", 100));
		employees.add(new Employee("D", "D", 100));
		employees.add(new Employee("E", "E", 100));
		employees.add(new Employee("F", "F", 100));
		configData.setEmployees(employees);
		Config.setDataForTests(configData);
		
		PlannedMonth plannedMonth = new SchedulerMonth(new Date(0), new Date(3*24*60*60*1000), "Test").schedule(strategy);
		assertEquals(plannedMonth.getPlannedDays().size(), 4);
	}
	
	@Test
	public void shouldPlanTheDaysContinuously() throws SchedulingCouldNotBeFinishedException {
		ConfigData configData = new ConfigData();
		configData.initDefaultValues();
		
		ArrayList<Employee> employees = new ArrayList<>();
		employees.add(new Employee("A", "A", 100));
		employees.add(new Employee("B", "B", 100));
		employees.add(new Employee("C", "C", 100));
		employees.add(new Employee("D", "D", 100));
		employees.add(new Employee("E", "E", 100));
		employees.add(new Employee("F", "F", 100));
		configData.setEmployees(employees);
		Config.setDataForTests(configData);
		
		PlannedMonth plannedMonth = new SchedulerMonth(new Date(0), new Date(3*24*60*60*1000), "Test").schedule(strategy);
		Date[] dates = plannedMonth.getPlannedDays().keySet().toArray(new Date[0]);
		assertEquals(dates[1].getTime() - dates[0].getTime(), 24*60*60*1000);
		assertEquals(dates[2].getTime() - dates[0].getTime(), 2*24*60*60*1000);
		assertEquals(dates[3].getTime() - dates[0].getTime(), 3*24*60*60*1000);
	}

}
