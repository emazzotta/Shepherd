package com.shepherd.zhaw.scheduling;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.config.ShiftType;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * The planning of a single day
 */
public class PlannedDay {
	private Date date;
	private HashMap<ShiftType, ArrayList<Employee>> plannedEmployees;
	private int weight = 0;
	
	public PlannedDay(Date date) {
		this.date = date;
		plannedEmployees = new HashMap<ShiftType, ArrayList<Employee>>(Config.getData().getShiftTypes().length); 
	}

	/**
	 * Add a employee to the given planning
	 * @param the shift the employee should be planned in
	 * @param the employee to be added to the given planning
	 */
	public void addPlannedEmployee(ShiftType shiftType, Employee employee) {
		if (plannedEmployees.containsKey(shiftType)) {
			ArrayList<Employee> employees = plannedEmployees.get(shiftType);
			employees.add(employee);
			plannedEmployees.put(shiftType, employees);
		} else {
			ArrayList<Employee> employees = new ArrayList<>(shiftType.getNeededWorkforcePercent() / 100);
			employees.add(employee);
			plannedEmployees.put(shiftType, employees);
		}
	}
	
	/**
	 * Returns all employees planned in a specific shift
	 * @param the shift the employees should be planned in
	 * @return all employees planned in the given shift
	 */
	public ArrayList<Employee> getPlannedEmployeesByShiftType(ShiftType shiftType) {
		ArrayList<Employee> employeesByShiftType;
		if (plannedEmployees.containsKey(shiftType)) {
			employeesByShiftType = plannedEmployees.get(shiftType); 
		} else {
			employeesByShiftType = new ArrayList<Employee>();
		}
		return employeesByShiftType;
	}
	
	/**
	 * Returns all planned employees 
	 * @return all planned employees
	 */
	public ArrayList<Employee> getAllPlannedEmployees() {
		ArrayList<Employee> employees = new ArrayList<Employee>(); 
		for(ShiftType shiftType : Config.getData().getShiftTypes()) {
			ArrayList<Employee> employeesByShift =  plannedEmployees.get(shiftType);
			if (employeesByShift != null) {
				employees.addAll(employeesByShift);
			}
		}
		return employees;
	}

	public Date getDate() {
		return date;
	}
	
	public void addWeight(int additionalWeight) {
		weight += additionalWeight;
	}
	
	public int getWeight() {
		return weight;
	}
}
