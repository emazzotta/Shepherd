package com.shepherd.zhaw.constraints.mandatory;

import com.shepherd.zhaw.config.ShiftType;
import com.shepherd.zhaw.constraints.ConstraintInput;
import com.shepherd.zhaw.scheduling.PlannedDay;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Makes sure an employee may not be scheduled for morning shift after a night shift.
 */
public class PreventMorningShiftAfterNightShiftConstraint extends MandatoryConstraint {

    /**
     * Returns true if the shift to be planned conforms with this constraint
     * @param input contains information about all shifts planned so far and the current shift to be planned
     * @return if shift to be planned is conform with this constraint
     */
	@Override
	public boolean isValid(ConstraintInput input) {
		
        
        ArrayList<PlannedDay> plannedDays = input.getPlannedMonth().getPlannedDaysByEmployee(input.getEmployee());
        if(plannedDays.size() < 2 || input.getShiftType().getShortName() != 'F') {
            return true;
        }

        // Shift to plan is morning, return false if the same employee was planned with night shift yesterday
        PlannedDay lastDay = plannedDays.get(plannedDays.size() - 1);

        ShiftType nightShift = (ShiftType) Arrays.stream(input.getShiftTypes()).filter(s -> s.getShortName() == 'N').toArray()[0];

        return !lastDay.getPlannedEmployeesByShiftType(nightShift).contains(input.getEmployee());
    }

}
