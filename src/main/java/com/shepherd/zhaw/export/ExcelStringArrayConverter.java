package com.shepherd.zhaw.export;

import org.apache.poi.ss.usermodel.IndexedColors;

public class ExcelStringArrayConverter {

	public static ExcelString[][] convert(String[][] textArray, IndexedColors color) {
		int longestRow = 0;
		for (String[] row : textArray) {
			if (row.length > longestRow) {
				longestRow = row.length;
			}
		}

		ExcelString[][] excelString = new ExcelString[textArray.length][longestRow];

		for (int i = 0; i < textArray.length; i++) {
			for (int j = 0; j < longestRow; j++) {
				if (textArray[i].length > j) {
					excelString[i][j] = new ExcelString(textArray[i][j], color);
				} else {
					excelString[i][j] = new ExcelString("", color);
				}
			}
		}

		return excelString;
	}
}
