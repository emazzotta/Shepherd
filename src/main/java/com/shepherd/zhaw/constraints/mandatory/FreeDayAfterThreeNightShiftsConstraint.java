package com.shepherd.zhaw.constraints.mandatory;

import com.shepherd.zhaw.config.ShiftType;
import com.shepherd.zhaw.constraints.ConstraintInput;
import com.shepherd.zhaw.scheduling.PlannedDay;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


/**
 * Makes sure that an employee gets one free day after three night shifts
 */
public class FreeDayAfterThreeNightShiftsConstraint extends MandatoryConstraint {


	/**
	 * Returns true if the shift to be planned conforms with this constraint
	 * @param input contains information about all shifts planned so far and the current shift to be planned
	 * @return if shift to be planned is conform with this constraint
	 */
	@Override
	public boolean isValid(ConstraintInput input) {
		
        HashMap<Date, PlannedDay> plannedDays = input.getPlannedMonth().getPlannedDays();
        if(plannedDays.size() < 3) {
            return true;
        }

        Date lastDate = dayBefore(input.getDate());
        ShiftType nightShift = (ShiftType) Arrays.stream(input.getShiftTypes()).filter(s -> s.getShortName() == 'N').toArray()[0];

        for(int i = 1; i <= 3; i++) {
        	if(!plannedDays.containsKey(lastDate))
        		return true;
        	
        	if(!plannedDays.get(lastDate).getPlannedEmployeesByShiftType(nightShift).contains(input.getEmployee()))
        		return true;
        	
        	lastDate = dayBefore(lastDate);
        }

        return false;
        
	}

	/**
	 * Helper method to return the day before
	 * @param day the day to find yesterday of
	 * @return yesterday of day
	 */
	private Date dayBefore(Date day) {
		Date yesterday = new Date();
		yesterday.setTime( day.getTime() - 1000*60*60*24);
		return yesterday;
	}

}
