package com.shepherd.zhaw.constraints.mandatory;

import com.shepherd.zhaw.constraints.ConstraintInput;
import com.shepherd.zhaw.scheduling.PlannedDay;

import java.util.ArrayList;
import java.util.Date;

/**
 * Enforces that no employee is scheduled for more than (maxSubsequentDays) consecutive days.
 */
public class MaxSubsequentWorkDayConstraint extends MandatoryConstraint {

    /**
     * Returns true if the shift to be planned conforms with this constraint
     * @param input contains information about all shifts planned so far and the current shift to be planned
     * @return if shift to be planned is conform with this constraint
     */
    @Override
    public boolean isValid(ConstraintInput input) {

        int maxSubsequentDays = getSetting("maxSubsequentWorkDays", Double.class).intValue();
        
        ArrayList<PlannedDay> plannedDays = input.getPlannedMonth().getPlannedDaysByEmployee(input.getEmployee());
        if(plannedDays.size() < maxSubsequentDays) {
            return true;
        }

        Date lastDate = plannedDays.get(plannedDays.size() - 1).getDate();

        for(int i = 2; i < maxSubsequentDays; i++) {
            Date newDate = plannedDays.get(plannedDays.size() - i).getDate();

            boolean moreThanOneDayBetween = (lastDate.getTime() - newDate.getTime()) > (1000*60*60*24);
            lastDate = newDate;
            if(moreThanOneDayBetween)
                return true;
        }

        return false;
    }
}
