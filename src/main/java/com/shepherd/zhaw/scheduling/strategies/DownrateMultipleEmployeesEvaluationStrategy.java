package com.shepherd.zhaw.scheduling.strategies;

import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.scheduling.TemporaryAssignment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *	Evaluation strategy which down rates the given employee at the start of the planning
 */
public class DownrateMultipleEmployeesEvaluationStrategy extends EvaluationStrategy implements Serializable {
	private ArrayList<Employee> employeesToDownrate;
	private final static int downrateFator = 2;
	private final static Logger LOGGER = Logger.getLogger("scheduler");
    private static final long serialVersionUID = 1;
	
	public DownrateMultipleEmployeesEvaluationStrategy(ArrayList<Employee> employeesToDownrate) {
		this.employeesToDownrate = employeesToDownrate;
	}
	
	/**
	 * Compares two TemporaryAssignment-Object based on the strategy to down rate the given employee for the first two weeks
	 * @param the first TemporaryAssignment-Object to compare
	 * @param the second TemporaryAssignment-Object to compare
	 */
	@Override
	public int compare(TemporaryAssignment ta1, TemporaryAssignment ta2) {
		int daysTillEnd = 7 - plannedMonth.getPlannedDays().size();
		if (daysTillEnd < 0) daysTillEnd = 0;
		
		double ta1WorktimeBalance = plannedMonth.getWorktimeBalance(ta1.getEmployee()) + ta1.getEmployee().getWorktimeBalance();
		double ta2WorktimeBalance = plannedMonth.getWorktimeBalance(ta2.getEmployee()) + ta2.getEmployee().getWorktimeBalance();  

		if (Double.compare(downrateGivenEmployee(ta1, daysTillEnd) + ta1WorktimeBalance, downrateGivenEmployee(ta2, daysTillEnd) + ta2WorktimeBalance) == 1) {
			return 1;
		} else if (Double.compare(downrateGivenEmployee(ta1, daysTillEnd) + ta1WorktimeBalance, downrateGivenEmployee(ta2, daysTillEnd) + ta2WorktimeBalance) == -1 || !ta1.equals(ta2)) {
			return -1;
		} else {
			return 0;
		}
	}
	
	private int downrateGivenEmployee(TemporaryAssignment ta, int daysTillEnd) {
		int ratedWeight = ta.getAssignementWeight();
		if (employeesToDownrate.contains(ta.getEmployee())) {
			ratedWeight += daysTillEnd * downrateFator;
		} 
		return ratedWeight;
	}

	/**
	 * Returns the name of the strategy
	 * @return the name of the strategy
	 */
	@Override
	public String getName() {
		return "Mehrere Mitarbeiter heruntergewichten";
	}
}
