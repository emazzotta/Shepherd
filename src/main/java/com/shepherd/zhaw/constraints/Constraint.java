package com.shepherd.zhaw.constraints;

import java.util.Hashtable;

import com.google.gson.Gson;

/**
 * All constraints inherit from this class. Handles serializing of settings and
 * contains generic information about a constraint.
 * This class should not be instantiated directly and thus should be marked as
 * abstract. The reason why it's not is that the JSON serializer requires classes
 * not to be abstract.
 */
public class Constraint {

	protected String name;
	protected String constraintType;
	protected String settings;

	public Constraint() {}

	/**
	 * Return the configured name of this constraint.
	 * @return name of this constraint
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of this constraint.
	 * @param name sets the constraint name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Return the type of this constraint (needed for dynamic instantiation through ConstraintFactory).
	 * @return type (full package + class name)
	 */
	public String getConstraintType() {
		return constraintType;
	}

	/**
	 * Set the type of this constraint
	 * @param constraintType type (full package + class name)
	 */
	public void setConstraintType(String constraintType) {
		this.constraintType = constraintType;
	}

	/**
	 * Returns all dynamic settings for this constraint.
	 * @return dynamic settings for this constraint as JSON-String
	 */
	public String getSettings() {
		return settings;
	}

	/**
	 * Returns the deserialized settings as Hashtable
	 * @return
	 */
	public Hashtable<String, Object> getDeserializedSettings() {
		return new Gson().fromJson(settings, Hashtable.class);
	}

	/**
	 * Return one setting from the settings HashTable
	 * @param key the key of the setting
	 * @param type type to interpret setting
	 * @param <T>
	 * @return the setting by key, casted to type
	 */
	public <T> T getSetting(String key, Class<T> type) {
		return (T) getDeserializedSettings().get(key);
	}

	/**
	 * Set settings for this constraint.
	 * @param settings Settings as JSON-String
	 */
	public void setSettings(String settings) {
		this.settings = settings;
	}

}
