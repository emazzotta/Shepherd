package com.shepherd.zhaw;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.shepherd.zhaw.ui.helper.ShepherdScreen;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Shepherd, Main Application
 */
public class Shepherd extends Application {

	private static final transient Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static final String ALLOWED_EXTENSION = "*.xlsx";
	public static final String SCREEN_TITLE = "Shepherd";
	public static final String AGB_PATH = "/agb/Shepherd_AGB.pdf";

	private Stage primaryStage;
	private BorderPane rootLayout;

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.getIcons().add(new Image("/logo/shepherd_logo_200-200.png"));
		this.primaryStage.setTitle(SCREEN_TITLE);
		initRootLayout();
		showStartScreen();
	}

	private void initRootLayout() {
		try {
			FXMLLoader loader = ShepherdScreen.getLoader(ShepherdScreen.ROOT_LAYOUT);
			rootLayout = loader.load();
			primaryStage.setScene(new Scene(rootLayout));
			primaryStage.setResizable(false);
			primaryStage.show();
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "RootLayout error", e);
		}
	}

	private void showStartScreen() {
		try {
			FXMLLoader loader = ShepherdScreen.getLoader(ShepherdScreen.START_SCREEN);
			AnchorPane startScreen = loader.load();
			rootLayout.setCenter(startScreen);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "StartScreen error", e);
		}
	}

	public static void main(String[] args) {

		// Temporary fix for Combobox-Bug
		// http://stackoverflow.com/questions/31786980/javafx-combobox-not-responding-on-windows-10
		System.setProperty("glass.accessible.force", "false");

		// Init Logger
		try {
			LogManager.getLogManager().readConfiguration(Shepherd.class.getResourceAsStream("/logging.properties"));
		} catch (final IOException e) {
			e.printStackTrace();
		}

		// Launch FX-Application
		launch(args);

	}
}
