package com.shepherd.zhaw.config;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConfigTest {

    @Test
    public void shouldGetTheSameDataInstanceTwice() {
        assertEquals(Config.getData(), Config.getData());
    }
}
