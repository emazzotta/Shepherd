package com.shepherd.zhaw.export;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.ConfigData;
import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.config.ShiftType;
import com.shepherd.zhaw.scheduling.PlannedDay;
import com.shepherd.zhaw.scheduling.PlannedMonth;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Exports the schedule into a xlsx file
 */
public class ExcelExport {

	public static void write(PlannedMonth plannedMonth, String path) {
		HashMap<Employee, Integer> alreadyAddedEmployees = new HashMap<>();
		HashMap<Date, PlannedDay> plannedDays = plannedMonth.getPlannedDays();
		List<Date> sortedDate = plannedDays.keySet().stream().sorted(Comparator.comparingLong(Date::getTime))
				.collect(Collectors.toList());

		ExcelString[][] excelStrings = new ExcelString[plannedMonth.getAllEmployees().size() + 2][sortedDate.size()
				+ 1];
		excelStrings[0][0] = new ExcelString(plannedMonth.getName(), IndexedColors.AUTOMATIC, 0, 22);
		int lastUsedRow = 1;

		for (int i = 0; i < sortedDate.size(); i++) {
			int currentColumn = i + 1;
			Date currentDate = sortedDate.get(i);
			String formattedDate = new SimpleDateFormat("dd.MM.yyyy").format(currentDate);
			excelStrings[1][currentColumn] = new ExcelString(formattedDate, IndexedColors.AUTOMATIC, (short) 90);

			for (ShiftType shiftType : Config.getData().getShiftTypes()) {
				ArrayList<Employee> shiftEmployees = plannedDays.get(currentDate)
						.getPlannedEmployeesByShiftType(shiftType);
				if (shiftEmployees == null) {
					continue;
				}
				for (Employee employee : shiftEmployees) {
					if (alreadyAddedEmployees.get(employee) != null) {
						excelStrings[alreadyAddedEmployees.get(employee)][currentColumn] = new ExcelString(
								shiftType.getShortName(), getColorForShift(shiftType));
					} else {
						lastUsedRow++;
						excelStrings[lastUsedRow][0] = new ExcelString(employee.toString());
						excelStrings[lastUsedRow][currentColumn] = new ExcelString(shiftType.getShortName(),
								getColorForShift(shiftType));
						alreadyAddedEmployees.put(employee, lastUsedRow);
					}
				}
			}
		}

		write(markEmptySlots(excelStrings), path);
	}

	static void write(ExcelString data[][], String path) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Schichtplan");

		int longestRow = 0;
		for (ExcelString[] row : data) {
			if (row.length > longestRow) {
				longestRow = row.length;
			}
		}

		for (int i = 0; i < data.length; i++) {
			Row row = sheet.createRow(i);
			for (int j = 0; j < longestRow; j++) {
				Cell cell = row.createCell(j);
				if (data[i].length > j) {
					cell.setCellValue(data[i][j].getText());
					CellStyle style = workbook.createCellStyle();

					if (data[i][j].getColor() != IndexedColors.AUTOMATIC) {
						style.setFillPattern(CellStyle.SOLID_FOREGROUND);
						style.setFillForegroundColor(data[i][j].getColor().getIndex());
					}
					if (data[i][j].getFontHeightInPoints() != 11) {
						XSSFFont font = workbook.createFont();
						style.setFont(font);
						font.setFontHeightInPoints(data[i][j].getFontHeightInPoints());
					}

					style.setRotation(data[i][j].getRotation());
					cell.setCellStyle(style);
				}
			}
		}

		for (int i = 0; i < longestRow; i++) {
			sheet.setColumnWidth(i, 900);
		}

		sheet.setColumnWidth(0, 6000);
		sheet.createFreezePane(0, 2);

		try (FileOutputStream out = new FileOutputStream(new File(path))) {
			workbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static String[][] read(String path) {
		String data[][] = null;
		try (FileInputStream file = new FileInputStream(new File(path))) {
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);

			int iMax = sheet.getPhysicalNumberOfRows();
			for (int i = 0; i < iMax; i++) {
				Row currentRow = sheet.getRow(i);
				int jMax = currentRow.getLastCellNum();
				if (data == null) {
					data = new String[iMax][jMax];
				}
				for (int j = 0; j < jMax; j++) {
					Cell cell = currentRow.getCell(j);
					data[i][j] = cell.getStringCellValue();
				}
			}
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

	private static ExcelString[][] markEmptySlots(ExcelString[][] excelStrings) {
		for (int i = 0; i < excelStrings.length; i++) {
			for (int j = 0; j < excelStrings[0].length; j++) {
				if (excelStrings[i][j] == null) {
					excelStrings[i][j] = new ExcelString("");
				}
			}
		}
		return excelStrings;
	}

	private static IndexedColors getColorForShift(ShiftType shiftType) {
		switch (shiftType.getShortName()) {
		case ConfigData.SHIFT_EARLY_SHORT:
			return IndexedColors.LIGHT_BLUE;
		case ConfigData.SHIFT_LATE_SHORT:
			return IndexedColors.LIGHT_GREEN;
		case ConfigData.SHIFT_NIGHT_SHORT:
			return IndexedColors.LIGHT_ORANGE;
		default:
			return IndexedColors.WHITE;
		}
	}
}
