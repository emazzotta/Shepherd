package com.shepherd.zhaw.ui.model;

public class ConstraintTypeModel {

	private Class<?> clazz;

	public ConstraintTypeModel(Class<?> clazz) {
		this.clazz = clazz;
	}

	public ConstraintTypeModel(String clazz) {
		try {
			this.clazz = Class.forName(clazz);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	//
	public String getFull() {
		return clazz.getName();
	}

	//
	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	@Override
	public String toString() {
		return clazz.getSimpleName();
	}
}
