package com.shepherd.zhaw.util;

import java.io.File;

public class Util {
    public static void createDirectory(String directoryPath) {
        File directory = new File(directoryPath);
        if (!directory.exists()) {
            if(directory.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory creation failed");
            }
        }
    }
}
