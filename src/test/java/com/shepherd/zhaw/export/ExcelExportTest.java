package com.shepherd.zhaw.export;

import com.shepherd.zhaw.config.Config;
import com.shepherd.zhaw.config.Employee;
import com.shepherd.zhaw.scheduling.PlannedDay;
import com.shepherd.zhaw.scheduling.PlannedMonth;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ExcelExportTest {

    private File filePath;
    private String[][] data;
    private ExcelString[][] excelData;

    @Before
    public void setUp() throws IOException {
        filePath = new File("./excelTestFile.xlsx");
        deleteIfExists(filePath);
        data = new String[][] {
                {"Test1", "Test2", "Test3"},
                {},
                {"Test10", "Test10"},
                {"Test1", "Test2", "Test3", "Test1", "Test2", "Test3"}
        };
        excelData = ExcelStringArrayConverter.convert(data, IndexedColors.LIGHT_ORANGE);
    }

    @After
    public void tearDown() throws IOException {
        deleteIfExists(filePath);
    }

    private void deleteIfExists(File filePath) throws IOException {
        if (filePath.exists()) {
            FileUtils.forceDelete(filePath);
        }
    }

    @Test
    public void shouldWriteAndReadExcelFilesCorrectly() {
        ExcelExport.write(excelData, filePath.getPath());
        String resultData[][] = ExcelExport.read(filePath.getPath());

        for(int i=0;i<resultData.length;i++) {
            for(int j=0;j<resultData[0].length;j++) {
                if(j < data[i].length) {
                    assertEquals(data[i][j], resultData[i][j]);
                }
            }
        }
    }

    @Test
    public void shouldWriteExcelWithPlannedMonthCorrectly() {
        ExcelExport.write(getTestPlannedMonth(), filePath.getPath());
        String expectedResult[][] = {
                {"Juli 2017", "", "", ""},
                {"", "20.01.2017", "21.01.2017", "23.01.2017"},
                {"Emanuele Mazzotta (100%)", "F", "N", ""},
                {"Sebastian Brunner (100%)", "S", "", ""},
                {"Patrick Brunner (100%)", "", "N", ""},
        };
        assertArrayEquals(expectedResult, ExcelExport.read(filePath.getPath()));
    }

    private PlannedMonth getTestPlannedMonth() {
        Date firstDay = new GregorianCalendar(2017, Calendar.JANUARY, 20).getTime();
        Date secondDay = new GregorianCalendar(2017, Calendar.JANUARY, 21).getTime();
        Date thirdDay = new GregorianCalendar(2017, Calendar.JANUARY, 23).getTime();

        PlannedMonth plannedMonth = new PlannedMonth("Juli 2017");
        PlannedDay plannedDay1 = new PlannedDay(firstDay);
        PlannedDay plannedDay2 = new PlannedDay(secondDay);

        Employee employee1 = new Employee("Emanuele", "Mazzotta", 100);
        Employee employee2 = new Employee("Sebastian", "Brunner", 100);
        plannedDay1.addPlannedEmployee(Config.getData().getShiftTypes()[0], employee1);
        plannedDay1.addPlannedEmployee(Config.getData().getShiftTypes()[1], employee2);

        Employee employee3 = new Employee("Patrick", "Brunner", 100);
        plannedDay2.addPlannedEmployee(Config.getData().getShiftTypes()[2], employee3);
        plannedDay2.addPlannedEmployee(Config.getData().getShiftTypes()[2], employee1);

        plannedMonth.addPlannedDay(firstDay, plannedDay1);
        plannedMonth.addPlannedDay(secondDay, plannedDay2);
        plannedMonth.addPlannedDay(thirdDay, new PlannedDay(thirdDay));

        return plannedMonth;
    }
}
